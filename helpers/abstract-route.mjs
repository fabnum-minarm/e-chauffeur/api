import Router from '@koa/router';
import maskOutput from '../middlewares/mask-output';
import resolveRights from '../middlewares/check-rights';
import addFilter from '../middlewares/add-filter';
import initFilters from '../middlewares/init-filters';
import APIError from './api-error';

/**
 * Abstract function to add batch import from CSV to router
 *
 * @param {Model} Model the model to link CRUD
 * @param {Object} [options] Options
 * @param {String} [options.url=/batch] Path to the route
 * @param {Symbol|string} [options.right] Right able to access the route
 * @param {string[]|Symbol[]} [options.rights] Rights able to access the route
 * @param {Function[]} [options.middlewares] Middlewares
 * @param {Function} [options.main] Main function of the route
 * @param {Array} [options.refs] Data reference keys
 */
export function addBatchToRouter(Model, {
  url = '/batch', right, rights = [], middlewares = [], main, refs = [],
} = {}) {
  if (!right) {
    throw new APIError(403, 'Right should be defined');
  }
  this.post(
    url,
    ...[right]
      .concat(rights)
      .filter((r) => !!r)
      .map((r) => resolveRights(...[].concat(r))),
    ...middlewares,
    main || (async (ctx) => {
      await Model.createFromCSV({
        model: Model, refs, datas: ctx.file,
      });
      ctx.log.info(`${Model.modelName} batch has been created`);
      ctx.status = 204;
    }),
  );
}

/**
 * Abstract function to add creation to router
 *
 * @param {Model} Model the model to link CRUD
 * @param {Object} [options] Options
 * @param {String} [options.url=/] Path to the route
 * @param {Symbol|string} [options.right] Right able to access the route
 * @param {string[]|Symbol[]} [options.rights] Rights able to access the route
 * @param {Function} [options.main] Main function of the route
 * @param {Number} [options.successCode] HTTP Code to return on success
 * @param {Function[]} [options.middlewares] Middlewares
 */
export function addCreateToRouter(Model, {
  url = '/', right, rights = [], main, successCode = 200, middlewares = [],
} = {}) {
  if (!right) {
    throw new APIError(403, 'Right should be defined');
  }
  const autoGenId = !Model.schema.obj._id;
  this.post(
    url,
    ...[right]
      .concat(rights)
      .filter((r) => !!r)
      .map((r) => resolveRights(...[].concat(r))),
    maskOutput,
    ...middlewares,
    main || (async (ctx) => {
      const { request: { body } } = ctx;

      if (!autoGenId) {
        if (await Model.findById(body.id)) {
          ctx.log.error(`${Model.modelName} "${body.id}" already exists`);
          ctx.throw(
            409,
            ctx.translate(
              'mongoose.errors.AlreadyExists',
              { model: ctx.translate(`mongoose.models.${Model.modelName}`), id: body.id },
            ),
          );
        }

        Object.assign(body, { _id: body.id });
      }
      const document = await Model.create(body);
      ctx.status = successCode;

      if (successCode !== 204) {
        ctx.body = document;
      }
      ctx.log.info(`${Model.modelName} "${body.id}" has been created`);
    }),
  );
}

/**
 * Abstract function to add list to router
 *
 * @param {Model} Model the model to link CRUD
 * @param {Object} [options] Options
 * @param {String} [options.url=/] Path to the route
 * @param {Symbol|string} [options.right] Right able to access the route
 * @param {string[]|Symbol[]} [options.rights] Rights able to access the route
 * @param {Function} [options.main] Main function of the route
 * @param {Object} [options.filters] Query filters
 * @param {Function[]} [options.middlewares] Middlewares
 * @param {Boolean} [options.lean=true] Add mongoose wrapper when falsy
 */
export function addListToRouter(Model, {
  url = '/', right, rights = [], main, filters = {}, middlewares = [], lean = true,
} = {}) {
  if (!right) {
    throw new APIError(403, 'Right should be defined');
  }

  this.get(
    url,
    ...[right]
      .concat(rights)
      .filter((r) => !!r)
      .map((r) => resolveRights(...[].concat(r))),
    maskOutput,
    initFilters,
    ...Object.keys(filters).map((k) => addFilter(k, filters[k])),
    ...middlewares,
    main || (async (ctx) => {
      const { offset, limit } = ctx.parseRangePagination(Model);
      const [total, data] = await Promise.all([
        Model.countDocuments(ctx.filters),
        lean ? Model.find(ctx.filters).skip(offset).limit(limit).lean()
          : Model.find(ctx.filters).skip(offset).limit(limit),
      ]);

      ctx.log.info(
        {
          filters: ctx.filters, offset, limit, total,
        },
        `Find query in ${Model.modelName}`,
      );

      ctx.setRangePagination(Model, {
        total, offset, count: data.length, limit,
      });
      ctx.body = data;
    }),
  );
}

/**
 * Abstract function to add get to router
 *
 * @param {Model} Model the model to link CRUD
 * @param {Object} [options] Options
 * @param {String} [options.url=/:id] Path to the route
 * @param {Symbol|string} [options.right] Right able to access the route
 * @param {string[]|Symbol[]} [options.rights] Rights able to access the route
 * @param {string} [options.paramId=id] Assign ID parameter according to his name in the path
 * @param {Function} [options.main] Main function of the route
 * @param {Function[]} [options.middlewares] Middlewares
 * @param {Function[]} [options.preMiddlewares] Middlewares to execute before default middlewares
 * @param {Boolean} [options.lean=true] Add mongoose wrapper when falsy
 */
export function addGetToRouter(Model, {
  paramId = 'id', url = `/:${paramId}`, right, rights = [], main, middlewares = [], preMiddlewares = [], lean = true,
} = {}) {
  this.get(
    url,
    ...preMiddlewares,
    ...[right]
      .concat(rights)
      .filter((r) => !!r)
      .map((r) => resolveRights(...[].concat(r))),
    maskOutput,
    ...middlewares,
    main || (async (ctx) => {
      const { params } = ctx;
      const id = params[paramId];
      try {
        if (lean) {
          ctx.body = await Model.findById(id).lean();
        } else {
          ctx.body = await Model.findById(id);
        }
        if (!ctx.body) {
          ctx.throw_and_log(404, `${Model.modelName} "${id}" not found`);
        }
        ctx.log.info(`Find ${Model.modelName} with "${id}"`);
      } catch (e) {
        ctx.throw_and_log(404, `${Model.modelName} "${id}" not found`);
      }
    }),
  );
}

/**
 * Abstract function to add deletion to router
 *
 * @param {Model} Model the model to link CRUD
 * @param {Object} [options]
 * @param {string} [options.paramId=id] Assign ID parameter according to his name in the path
 * @param {String} [options.url=/:id] Path to the route
 * @param {Symbol|string} [options.right] Right able to access the route
 * @param {string[]|Symbol[]} [options.rights] Rights able to access the route
 * @param {Function} [options.main] Main function of the route
 * @param {Function[]} [options.middlewares] Middlewares
 */
export function addDeleteToRouter(Model, {
  paramId = 'id', url = `/:${paramId}`, right, rights = [], main, middlewares = [],
} = {}) {
  this.del(
    url,
    ...[right]
      .concat(rights)
      .filter((r) => !!r)
      .map((r) => resolveRights(...[].concat(r))),
    ...middlewares,
    main || (async (ctx) => {
      const { params } = ctx;
      const id = params[paramId];
      await Model.deleteOne({ _id: id });
      ctx.log.info(`${Model.modelName} "${id}" has been removed`);
      ctx.status = 204;
    }),
  );
}

/**
 * Abstract function to add update to router
 *
 * @param {Model} Model the model to link CRUD
 * @param {Object} [options]
 * @param {string} [options.paramId=id] Assign ID parameter according to his name in the path
 * @param {String} [options.url=/:id] Path to the route
 * @param {Symbol|string} [options.right] Right able to access the route
 * @param {string[]|Symbol[]} [options.rights] Rights able to access the route
 * @param {Function} [options.main] Main function of the route
 * @param {Function[]} [options.middlewares] Middlewares
 * @param {Function[]} [options.preMiddlewares] Middlewares executed before default middlewares
 */
export function addUpdateToRouter(Model, {
  paramId = 'id', url = `/:${paramId}`, right, rights = [], main, middlewares = [], preMiddlewares = [],
} = {}) {
  this.patch(
    url,
    ...preMiddlewares,
    ...[right]
      .concat(rights)
      .filter((r) => !!r)
      .map((r) => resolveRights(...[].concat(r))),
    maskOutput,
    ...middlewares,
    main || (async (ctx) => {
      const { request: { body }, params } = ctx;
      const id = params[paramId];
      const model = await Model.findById(id);

      model.set(body);
      ctx.body = await model.save();
      ctx.log.info(
        { body },
        `${Model.modelName} "${id}" has been modified`,
      );
    }),
  );
}

/**
 * Abstract function to add many features to router (CRUD generation)
 *
 * @param {Model} Model the model to link CRUD
 * @param {Object} [options]
 * @param {module:koa-router|Router|undefined} [options.router] Target router
 * @param {Object|undefined} [options.create] Options for the create abstract route
 * @param {Object|undefined} [options.list] Options for the list abstract route
 * @param {Object|undefined} [options.get] Options for the get abstract route
 * @param {Object|undefined} [options.remove] Options for the remove abstract route
 * @param {Object|undefined} [options.update] Options for the update abstract route
 * @param {Object|undefined} [options.batch] Options for the batch abstract route
 * @returns {module:koa-router|Router}
 */
export default (Model, {
  router, create, list, get, delete: remove, update, batch,
} = {}) => {
  const routerInstance = router || new Router();
  if (create) {
    addCreateToRouter.call(routerInstance, Model, create);
  }
  if (list) {
    addListToRouter.call(routerInstance, Model, list);
  }
  if (get) {
    addGetToRouter.call(routerInstance, Model, get);
  }
  if (remove) {
    addDeleteToRouter.call(routerInstance, Model, remove);
  }
  if (update) {
    addUpdateToRouter.call(routerInstance, Model, update);
  }
  if (batch) {
    addBatchToRouter.call(routerInstance, Model, batch);
  }
  return routerInstance;
};
