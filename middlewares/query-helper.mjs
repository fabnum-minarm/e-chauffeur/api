/**
 * Middleware factory, to ensure that filters exists, or return a 400
 *
 * @param {...Object} requiredFilters
 * @returns {function(...[*])}
 */
export const ensureThatFiltersExists = (...requiredFilters) => async (ctx, next) => {
  ctx.assert(ctx.query, 400, 'No query string found');
  const { filters } = ctx.query;
  ctx.assert(filters, 400, '"filters" not found in query string');
  requiredFilters.forEach((key) => ctx.assert(filters[key], 400, `"${key}" filter is required`));
  await next();
};

/**
 * Middleware factory, to parse all filters via JSON, or return a 400
 *
 * @param {...Object} filtersToParse
 * @returns {function(...[*])}
 */
export const filtersToObject = (...filtersToParse) => async (ctx, next) => {
  ctx.assert(ctx.query, 400, 'No query string found');
  const { filters } = ctx.query;
  ctx.assert(filters, 400, '"filters" not found in query string');
  filtersToParse.forEach((key) => { if (filters[key]) ctx.filters[key] = JSON.parse(filters[key]); });
  await next();
};

/**
 * Middleware factory, to cast filters via function

 * @param {string} filterKey
 * @param {string} paramsKey
 * @param {function(...[*])} cast
 * @returns {function(...[*])}
 */
export const filtersFromParams = (filterKey, paramsKey, cast = (e) => e) => async (ctx, next) => {
  if (ctx.params[paramsKey]) {
    ctx.filters[filterKey] = cast(ctx.params[paramsKey]);
  }
  await next();
};
