import { nanoid } from 'nanoid';
import Poi from '../../models/poi';

export const generateDummyPoi = (params) => ({
  _id: nanoid(),
  label: 'Test',
  ...params,
});

export const createDummyPoi = async (params) => {
  const dummyPoi = generateDummyPoi(params);
  return Poi.create(dummyPoi);
};

export default Poi;
