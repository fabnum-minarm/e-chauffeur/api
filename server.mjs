import http from 'http';
import { Server } from 'socket.io';
import redisAdapter from 'socket.io-redis';
import services from './services';
import app from './app';
import './prometheus-exporter';
import io from './io';
import config from './services/config';
import { pubClient, subClient } from './services/redis';

(async () => {
  await services;
  const server = http.createServer(app.callback());

  const userWebsiteDomain = config.get('user_website_url');
  let cors;
  if (userWebsiteDomain) {
    const { host } = new URL(userWebsiteDomain);
    cors = {
      credentials: true,
      origin(origin, callback) {
        if (!origin) {
          return callback(null, true);
        }
        const originHost = new URL(origin);
        const endsWithHostName = originHost && originHost.host && originHost.host.endsWith(host);
        if (endsWithHostName) {
          return callback(null, true);
        }
        return callback(new Error('CORS not allowed'));
      },
      methods: ['GET', 'POST'],
    };
  }

  app.io = new Server(server, {
    serveClient: false,
    adapter: (pubClient && subClient) ? redisAdapter({ pubClient, subClient }) : undefined,
    cors,
  });
  io(app.io);
  server.listen(config.get('port') || 1337);
})();
