/**
 * Middleware to add search filter
 *
 * @param {Object} ctx KoaJS context
 * @param {function} next KoaJS next
 * @returns {Promise<void>}
 */
const searchQueryMiddleware = async (ctx, next) => {
  const searchParams = ctx.filters;
  if (ctx.query && ctx.query.search) {
    searchParams.$text = { $search: ctx.query.search };
  }
  ctx.filters = searchParams;
  await next();
};

export default searchQueryMiddleware;
