export const CAMPUS_MODEL_NAME = 'Campus';
export const CAR_MODEL_MODEL_NAME = 'CarModel';
export const CAR_MODEL_NAME = 'Car';
export const CATEGORY_MODEL_NAME = 'Category';
export const GEO_TRACKING_MODEL_NAME = 'GeoTracking';
export const NOTIFICATION_DEVICE_MODEL_NAME = 'NotificationDevice';
export const PHONE_MODEL_MODEL_NAME = 'PhoneModel';
export const PHONE_MODEL_NAME = 'Phone';
export const POI_MODEL_NAME = 'Poi';
export const RATING_MODEL_NAME = 'Rating';
export const RIDE_MODEL_NAME = 'Ride';
export const SHUTTLE_FACTORY_MODEL_NAME = 'ShuttleFactory';
export const SHUTTLE_MODEL_NAME = 'Shuttle';
export const STEP_MODEL_NAME = 'Step';
export const TIME_SLOT_MODEL_NAME = 'TimeSlot';
export const USER_MODEL_NAME = 'User';

export const CAMPUS_COLLECTION_NAME = 'campuses';
export const CAR_COLLECTION_NAME = 'cars';
export const CAR_MODEL_COLLECTION_NAME = 'car-models';
export const CATEGORY_COLLECTION_NAME = 'categories';
export const GEO_TRACKING_COLLECTION_NAME = 'geo-tracking';
export const NOTIFICATION_DEVICE_COLLECTION_NAME = 'notification-devices';
export const PHONE_COLLECTION_NAME = 'phones';
export const PHONE_MODEL_COLLECTION_NAME = 'phone-models';
export const POI_COLLECTION_NAME = 'pois';
export const RATING_COLLECTION_NAME = 'ratings';
export const RIDE_COLLECTION_NAME = 'rides';
export const SHUTTLE_COLLECTION_NAME = 'shuttles';
export const SHUTTLE_FACTORY_COLLECTION_NAME = 'shuttle-factories';
export const STEP_COLLECTION_NAME = 'steps';
export const TIME_SLOT_COLLECTION_NAME = 'time-slots';
export const USER_COLLECTION_NAME = 'users';

export const CAR_MODEL_DASHED_NAME = 'car-model';
export const PHONE_MODEL_DASHED_NAME = 'phone-model';
export const SHUTTLE_FACTORY_DASHED_NAME = 'shuttle-factory';
export const TIME_SLOT_DASHED_NAME = 'time-slot';

export const MONTHLY = 'monthly';
export const WEEKLY = 'weekly';
