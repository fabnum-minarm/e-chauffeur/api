import config from '../../services/config';

export function withinFilter(rawStart, rawEnd, f = {}) {
  const queryFilter = f;
  const start = new Date(rawStart);
  const end = new Date(rawEnd);
  queryFilter.$or = [
    {
      start: {
        $lte: start,
      },
      end: {
        $gte: start,
        $lte: end,
      },
    },
    {
      start: {
        $gte: start,
        $lte: end,
      },
      end: {
        $gte: end,
      },
    },
    {
      start: {
        $lte: start,
      },
      end: {
        $gte: end,
      },
    },
    {
      start: {
        $gte: start,
        $lte: end,
      },
      end: {
        $gte: start,
        $lte: end,
      },
    },
  ];
  return queryFilter;
}

export function findWithin(after, before, filters = {}, ...rest) {
  return this.find(
    this.filtersWithin(after, before, filters),
    ...rest,
  );
}

export function countDocumentsWithin(after, before, filters = {}, ...rest) {
  return this.countDocuments(
    this.filtersWithin(after, before, filters),
    ...rest,
  );
}

export function compareTokens(token) {
  return this.token && token && this.token === token;
}

export function getClientURL() {
  return `${config.get('user_website_url')}/${this.id}?token=${this.token}`;
}

export function generateCampusFilter(campuses) {
  return campuses.length > 0 ? { $or: [].concat(campuses).map((campusId) => ({ 'campus._id': campusId })) } : {};
}
