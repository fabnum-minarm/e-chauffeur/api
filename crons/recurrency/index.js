import mongoose from 'mongoose';
import pino from 'pino';
import '../../models/role';
import timeSlotRecurrency from './time-slots';
import shuttleRecurrency from './shuttles';
import MongooseService from '../../services/mongoose';
import config from '../../services/config';

const logger = pino();

(async () => {
  const hrstart = process.hrtime();
  await MongooseService(config.get('mongodb'));

  await Promise.all([
    timeSlotRecurrency(logger),
    shuttleRecurrency(logger),
  ]);

  await mongoose.disconnect();
  const hrend = process.hrtime(hrstart);
  logger.info(`Done in ${hrend[0]}s ${hrend[1] / 1000000}ms`);
  process.exit(0);
})();
