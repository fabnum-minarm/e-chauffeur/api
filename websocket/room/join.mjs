import jwt from 'jsonwebtoken';
import pino from 'pino';
import Ride from '../../models/ride';
import { CANCELED_STATUSES, DELIVERED } from '../../models/states/statuses';
import { secretTokens } from '../../services/secret-tokens';

const logger = pino();

export default (socket) => {
  socket.on('roomJoinDriver', (token) => {
    let user;
    if (secretTokens.some((secret) => {
      try {
        user = jwt.verify(token.replace('Bearer ', ''), secret);
        return true;
      } catch (e) {
        return false;
      }
    })) {
      if (user.id) {
        socket.join(`driver/${user.id}`);
        // eslint-disable-next-line no-param-reassign
        socket.driverId = user.id;
      }
    } else {
      logger.info('Invalid secret');
    }
  });

  socket.on('roomJoinAdmin', (token, campus = {}) => {
    if (secretTokens.some((secret) => {
      try {
        jwt.verify(token.replace('Bearer ', ''), secret);
        return true;
      } catch (e) {
        return false;
      }
    })) {
      if (campus.id) {
        socket.join(`campus/${campus.id}`);
      }
    } else {
      logger.info('Invalid secret');
    }
  });

  socket.on('roomJoinRide', async (ride) => {
    if (ride.id && ride.token) {
      const rde = await Ride.findById(Ride.castId(ride.id));
      if (rde.token === ride.token && rde.status !== DELIVERED && CANCELED_STATUSES.indexOf(rde.status) === -1) {
        socket.join(`ride/${ride.id}`);
      }
    }
  });

  socket.on('roomLeaveAll', () => {
    socket.leaveAll();
  });
};
