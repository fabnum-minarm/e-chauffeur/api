import { nanoid } from 'nanoid';
import Campus from '../../models/campus';

const randomLoc = () => Math.floor(Math.random() * Math.floor(100));

export const generateDummyCampus = (params) => ({
  _id: nanoid(),
  name: nanoid(),
  location: {
    type: 'Point',
    coordinates: [randomLoc(), randomLoc()],
  },
  workedDays: [1, 2, 3, 4, 5, 6, 7],
  workedHours: {
    start: 0,
    end: 24,
  },
  ...params,
});

export const createDummyCampus = async (params) => {
  const dummyCampus = generateDummyCampus(params);
  return Campus.create(dummyCampus);
};

export default Campus;
