import mongoose from 'mongoose';
import { CAR_MODEL_NAME, RIDE_MODEL_NAME } from './constants';
import {
  CANCELED_STATUSES, CREATED, DELIVERED, VALIDATED,
} from '../states/statuses';

export default async (type, displacement) => {
  const creationStatus = type === RIDE_MODEL_NAME ? VALIDATED : CREATED;
  const Car = mongoose.model(CAR_MODEL_NAME);

  if (displacement.status === creationStatus) {
    await Car
      .updateMany(
        { _id: { $ne: displacement.car._id }, 'unavailabilities.displacement._id': displacement._id },
        { $pull: { unavailabilities: { 'displacement._id': displacement._id } } },
      );

    const unavailability = {
      start: displacement.start,
      end: displacement.end,
      'displacement._id': displacement._id,
      'driver._id': displacement.driver._id,
    };

    await Car.findOneAndUpdate(
      { _id: displacement.car._id },
      { $push: { unavailabilities: unavailability } },
    );
  } else if ([...CANCELED_STATUSES, DELIVERED].includes(displacement.status)) {
    await Car.findOneAndUpdate(
      { _id: displacement.car._id },
      { $pull: { unavailabilities: { 'displacement._id': displacement._id } } },
    );
  }
};
