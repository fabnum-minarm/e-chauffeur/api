import mongoose from 'mongoose';
import Luxon from 'luxon';
import createdAtPlugin from './helpers/created-at';
import addCSVContentPlugin from './helpers/add-csv-content';
import { CAR_COLLECTION_NAME, CAR_MODEL_NAME } from './helpers/constants';
import CarModel from './car-model';
import APIError from '../helpers/api-error';
import { CAR_LICENSE, PUBLIC_TRANSPORT_LICENSE } from './user';

const { Interval } = Luxon;
const { Schema, Types } = mongoose;

const CarSchema = new Schema({
  _id: { type: String, required: true },
  label: { type: String, required: true },
  model: {
    _id: { type: String, required: true },
    label: { type: String, required: true },
    capacity: { type: Number, required: true },
  },
  unavailabilities: [{
    displacement: {
      _id: {
        alias: 'displacement.id',
        type: Types.ObjectId,
        required: true,
      },
    },
    driver: {
      _id: {
        alias: 'driver.id',
        type: Types.ObjectId,
        required: true,
      },
    },
    start: {
      type: Date,
      required: true,
    },
    end: {
      type: Date,
      required: true,
    },
  }],
  campus: {
    _id: { type: String, required: true },
    name: String,
  },
});

CarSchema.plugin(createdAtPlugin);
CarSchema.plugin(addCSVContentPlugin);

CarSchema.pre('validate', async function beforeSave() {
  if (this.model && this.model._id) {
    const model = await CarModel.findById(this.model._id).lean();
    if (!model) {
      throw new APIError(404, 'Car model not found');
    }

    this.model = model;
  }
});

CarSchema.virtual('campus.id')
  .get(function get() {
    return this.campus._id;
  })
  .set(function set(id) {
    this.campus._id = id;
  });

CarSchema.virtual('model.id')
  .get(function get() {
    return this.model._id;
  })
  .set(function set(id) {
    this.model._id = id;
  });

CarSchema.index({
  _id: 'text',
  label: 'text',
  'campus._id': 'text',
  'campus.name': 'text',
  'model._id': 'text',
  'model.label': 'text',
});

CarSchema.methods.getAvailabilities = function isAvailable(start, end, events) {
  const interval = Interval.fromDateTimes(start, end);
  const eventsIntervals = Interval.merge(events.filter((e) => !!e.toInterval).map((e) => e.toInterval()));
  try {
    const intervals = [];
    const diff = interval.difference(...eventsIntervals);
    if (diff) {
      intervals.push(...diff);
    }
    return intervals;
  } catch (e) {
    return [];
  }
};

CarSchema.statics.generateLicenseFilter = function generateLicenseFilter(license) {
  const filter = {};
  const filterCondition = { $gt: 9 };
  if (license === PUBLIC_TRANSPORT_LICENSE) {
    filter['model.capacity'] = filterCondition;
  } else if (license === CAR_LICENSE) {
    filter['model.capacity'] = { $not: filterCondition };
  }
  return filter;
};

export default mongoose.model(CAR_MODEL_NAME, CarSchema, CAR_COLLECTION_NAME);
