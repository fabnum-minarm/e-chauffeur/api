FROM node:14 as base
FROM base as builder

RUN apt update && apt upgrade -y
RUN apt install -y \
    python \
    make \
    g++ \
    locales

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install --only=production

FROM base

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY --from=builder /usr/src/app/node_modules ./node_modules
COPY . /usr/src/app/
COPY ./config.json.dist ./config.json

EXPOSE 1337
EXPOSE 1338

ENTRYPOINT [ "npm" ]
CMD [ "run", "serve" ]
