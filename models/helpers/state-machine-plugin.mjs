import StateMachine from 'javascript-state-machine';

// Default behavior based on @rentspree/mongoose-state-machine
const generateDefaultInitCb = (stateMachine) => function defaultInitCb() {
  const state = {
    ...stateMachine,
  };
  state.init = this.status;
  StateMachine.apply(this, state);
};

export default (initCb = generateDefaultInitCb) => (schema, { stateMachine } = {}) => {
  schema.post('init', initCb(stateMachine));

  schema.pre('save', function updateState() {
    if (this.state) {
      this.status = this.state;
    }
  });
};
