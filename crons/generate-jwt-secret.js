import pino from 'pino';
import { pubNewSecret } from '../services/secret-tokens';
import { disconnectAllClients } from '../services/redis';

const log = pino();

(async () => {
  const hrstart = process.hrtime();

  try {
    if (await pubNewSecret()) {
      const hrend = process.hrtime(hrstart);
      log.info(`New secret sent to redis in ${hrend[0]}s ${hrend[1] / 1000000}ms`);
    }
  } finally {
    await disconnectAllClients();
    process.exit(0);
  }
})();
