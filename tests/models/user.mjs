import mongoose from 'mongoose';
import { nanoid } from 'nanoid';
import User from '../../models/user';

const { Types: { ObjectId } } = mongoose;

export const generateDummyUser = (params) => ({
  _id: new ObjectId(),
  email: `${nanoid()}@localhost`,
  password: nanoid(),
  ...params,
});

export const createDummyUser = async (params) => {
  const dummyUser = generateDummyUser(params);
  return User.create(dummyUser);
};

export default User;
