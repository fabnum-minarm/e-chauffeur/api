import Step from '../step';

export const CREATE = 'create';
export const VALIDATE = 'validation';
export const REJECT_BOUNDARY = 'rejection_boundary';
export const REJECT_CAPACITY = 'rejection_capacity';
export const START = 'start_up';
export const WAIT = 'stay';
export const PROGRESS = 'progress';
export const DELIVER = 'deliver';
export const STOP = 'stop';
export const END = 'end_up';
export const BOOK = 'book';
export const WARN = 'warn';
export const GET_IN = 'get_in';
export const GET_OUT = 'get_out';
export const CANCEL = 'void';
export const CANCEL_TECHNICAL = 'cancel_technical';
export const CANCEL_REQUESTED_CUSTOMER = 'cancel_requested_by_customer';
export const CANCEL_CUSTOMER_OVERLOAD = 'cancel_customer_overload';
export const CANCEL_CUSTOMER_MISSING = 'cancel_customer_missing';

export const deleteRelatedSteps = async (filter) => {
  const relatedSteps = await Step.find(filter);
  await Promise.all(relatedSteps.map((step) => step.remove()));
};
