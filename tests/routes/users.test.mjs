import chai from 'chai';
import ValidationError from 'mongoose/lib/error/validation';
import request, {
  generateSuperAdminJWTHeader,
  generateDriverJWTHeader,
  generateUserJWTHeader,
} from '../request';
import User, { createDummyUser, generateDummyUser } from '../models/user';
import {
  testCreate, testDelete, testList, testGet, testUpdate, testBatch,
} from '../helpers/crud';

const { expect } = chai;
const config = {
  route: '/users',
  generateDummyObject: generateDummyUser,
  cannotCall: [generateDriverJWTHeader],
  canCall: [generateSuperAdminJWTHeader],
};

describe('Test the users route', () => {
  let toDropAfter = [];
  function toDrop(...entities) {
    toDropAfter.push(...entities);
  }

  beforeEach(() => {
    toDropAfter = [];
  });

  afterEach(() => Promise.all(toDropAfter.map((entity) => entity.remove())));

  it(...testCreate(User, {
    ...config,
  }));

  it(...testList(User, {
    ...config,
  }));

  it(...testDelete(User, {
    ...config,
    route: ({ id }) => `${config.route}/${id}`,
  }));

  it(...testGet(User, {
    ...config,
    route: ({ id }) => `${config.route}/${id}`,
  }));

  it(...testUpdate(User, {
    ...config,
    route: ({ id }) => `${config.route}/${id}`,
  }));

  it(...testBatch(User, {
    ...config,
    route: `${config.route}/batch`,
    ref: 'email',
    queryParams: {},
  }));

  it('User should be able to edit self password/name', async () => {
    const user = await createDummyUser();
    toDrop(user);

    const resA = await request()
      .patch(`/users/${user.id}`)
      .set(...generateUserJWTHeader());
    expect(resA.statusCode).to.equal(403);

    const resB = await request()
      .patch(`/users/${user.id}`)
      .set('Authorization', `Bearer ${user.emitJWT()}`);
    expect(resB.statusCode).to.equal(200);
  });

  it('User should be able to delete his account', async () => {
    const user = await createDummyUser();
    toDrop(user);

    const resA = await request()
      .delete(`/users/${user.id}`)
      .set(...generateUserJWTHeader());
    expect(resA.statusCode).to.equal(403);

    const resB = await request()
      .delete(`/users/${user.id}`)
      .set('Authorization', `Bearer ${user.emitJWT()}`);
    expect(resB.statusCode).to.equal(204);
  });

  it('It should not be possible to register too long email', async () => {
    // Reason : It's causing an issue while saving cause of index limitation
    const forgedEmail = `${'azerty'.repeat(100)}@localhost`;
    try {
      expect(await User.create({ email: forgedEmail })).to.be.a('null');
    } catch (e) {
      expect(e).to.be.an.instanceof(ValidationError);
    }
  });

  it('It should not be possible to register comma separated emails list', async () => {
    // Reason : It was possible to register with this kind of email : "foo@bar.com;bar@foo.com"
    // where the first one was the attacker email, and the second one was the whitelisted email
    const forgedEmail = 'foo@localhost;bar@localhost';
    try {
      expect(await User.create({ email: forgedEmail })).to.be.a('null');
    } catch (e) {
      expect(e).to.be.an.instanceof(ValidationError);
    }
  });

  it('Register user multiple times should not return something else than 204', async () => {
    // Reason : uppercased email was returning duplicated key error
    const email = 'AZERTY@localhost';
    const dummyUser = generateDummyUser({ email });
    await User.create(dummyUser);
    const { statusCode } = await request()
      .post(config.route)
      .set('X-Send-Token', 'true')
      .send(dummyUser);
    expect(statusCode).to.equal(204);
    await User.findOneAndDelete({ email: email.toLowerCase() });
  });
});
