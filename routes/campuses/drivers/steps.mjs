import mongoose from 'mongoose';
import camelCase from 'lodash.camelcase';
import generateCRUD from '../../../helpers/abstract-route';
import Step, {
  STEP_DRIVE_PASSENGER,
  STEP_DROP_PASSENGER,
  STEP_GET_PASSENGER, STEP_GO_TO_STOP,
  STEP_WAIT_PASSENGER,
} from '../../../models/step';
import {
  CAN_LIST_CAMPUS_DRIVER_RIDE, CAN_REMOVE_CAMPUS_DRIVER_STEP,
} from '../../../models/rights';
import { ensureThatFiltersExists, filtersFromParams } from '../../../middlewares/query-helper';
import { cleanObject } from '../../../middlewares/mask-output';
import {
  DELIVER, PROGRESS, START, STOP, WAIT,
} from '../../../models/states/actions';
import { STARTED } from '../../../models/states/statuses';
import { RIDE_MODEL_NAME, SHUTTLE_MODEL_NAME } from '../../../models/helpers/constants';

const { Types: { ObjectId } } = mongoose;
function ioEmit(ctx, data, eventName = '', rooms = []) {
  let { app: { io } } = ctx;
  rooms.forEach((room) => {
    io = io.in(room);
  });
  io.emit(eventName, data);
}
const router = generateCRUD(Step, {
  list: {
    right: CAN_LIST_CAMPUS_DRIVER_RIDE,
    middlewares: [
      filtersFromParams('campus._id', 'campus_id'),
      filtersFromParams('driver._id', 'driver_id', (o) => new ObjectId(o)),
      ensureThatFiltersExists('after', 'before'),
    ],
    main: async (ctx) => {
      const { offset, limit } = ctx.parseRangePagination(Step);
      const after = new Date(ctx.query.filters.after);
      const before = new Date(ctx.query.filters.before);
      const [total, data] = await Promise.all([
        Step.countDocumentsWithin(after, before, ctx.filters),
        Step.findWithin(after, before, ctx.filters).skip(offset).limit(limit),
      ]);

      ctx.log.info(
        {
          filters: ctx.filters, offset, limit, total,
        },
        `Find query in ${Step.modelName}`,
      );

      ctx.setRangePagination(Step, {
        total, offset, count: data.length, limit,
      });
      ctx.body = data;
    },
  },
  delete: {
    right: CAN_REMOVE_CAMPUS_DRIVER_STEP,
    async main(ctx) {
      const { params } = ctx;
      const { id } = params;
      const step = await Step.findById(id);
      let action = null;
      switch (step.action) {
        case STEP_GET_PASSENGER:
          action = START;
          break;
        case STEP_WAIT_PASSENGER:
          action = WAIT;
          break;
        case STEP_DRIVE_PASSENGER:
          action = PROGRESS;
          break;
        case STEP_DROP_PASSENGER:
          action = DELIVER;
          break;
        case STEP_GO_TO_STOP:
          action = START;
          break;
        default:
          break;
      }

      if (action) {
        const [modelName, eventName] = step.type === 'ride'
          ? [RIDE_MODEL_NAME, 'rideUpdate']
          : [SHUTTLE_MODEL_NAME, 'shuttleUpdate'];
        const Model = mongoose.model(modelName);
        const model = await Model.findById(step[step.type]._id);
        if (modelName === 'Shuttle') {
          if (model.status === STARTED && action === START) {
            model.currentStopIndex += 1;
            action = STOP;
          }
        }

        if (model.cannot(action)) {
          ctx.throw_and_log(400, `State violation : ${step.type} cannot switch to "${action}"`);
        }
        await model[camelCase(action)]();
        await model.save();
        // @todo check socket events
        ioEmit(ctx, cleanObject(model), eventName, [
          `ride/${model._id}`,
          `campus/${model.campus._id}`,
        ]);
      }

      step.done = new Date();
      await step.save();
      ioEmit(ctx, [cleanObject(step)], 'stepsUpdate', [`driver/${step.driver._id}`]);
      ctx.log.info(`${Step.modelName} "${id}" has been done`);
      ctx.status = 204;
    },
  },
});

export default router;
