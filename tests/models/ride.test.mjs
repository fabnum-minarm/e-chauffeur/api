import chai from 'chai';
import Luxon from 'luxon';
import '../request';
import Ride from './ride';
import Step from './step';
import { createDummyCampus } from './campus';
import { createDummyPoi } from './poi';
import { VALIDATED } from '../../models/states/statuses';
import { createDummyCar } from './car';
import { createDummyCarModel } from './car-model';

const { DateTime } = Luxon;
const { expect } = chai;

describe('Test to generate steps from ride', () => {
  let toDropAfter;
  function toDrop(...entities) {
    toDropAfter.push(...entities);
  }

  beforeEach(async () => {
    toDropAfter = [];
  });

  afterEach(() => Promise.all(toDropAfter.map((entity) => entity.remove())));

  it('Should generate many steps on a ride', async () => {
    const campus = await createDummyCampus({ workedHours: { start: 0, end: 20 } });
    toDrop(campus);

    const departure = await createDummyPoi();
    const arrival = await createDummyPoi();
    toDrop(departure, arrival);

    const model = await createDummyCarModel();
    const car = await createDummyCar({ campus, model });
    toDrop(model, car);

    const start = DateTime.local().startOf('day').plus({ hours: 10 });
    const end = start.plus({ hours: 1 });
    const ride = await Ride.create({
      start, end, campus, departure, arrival, status: VALIDATED, car,
    });
    toDrop(ride);

    const steps = await Step.find({ 'ride._id': ride._id });
    toDrop(...steps);

    expect(steps).to.have.lengthOf(4);
  });
});
