import {
  generateAdminJWTHeader as originalGenerateAdminJWTHeader,
  generateDriverJWTHeader,
  generateSuperAdminJWTHeader,
} from '../request';
import { createDummyCampus, generateDummyCampus } from '../models/campus';
import { createDummyPhoneModel } from '../models/phone-model';
import Phone, { generateDummyPhone } from '../models/phone';
import {
  testCreate, testCreateUnicity, testDelete, testList, testGet, testUpdate, testBatch,
} from '../helpers/crud';

const campus = generateDummyCampus();

const generateAdminJWTHeader = originalGenerateAdminJWTHeader.bind(null, campus);

const config = {
  route: '/phones',
  queryParams: {
    filters: {
      campus: campus._id,
    },
  },
  async generateDummyObject() {
    const toDropLater = [];

    const dummyCampus = await createDummyCampus(campus);
    toDropLater.push(dummyCampus);

    const phoneModel = await createDummyPhoneModel();
    toDropLater.push(phoneModel);

    const dummyPhone = await generateDummyPhone({ campus: dummyCampus, model: phoneModel });

    return [dummyPhone, toDropLater];
  },
  cannotCall: [generateDriverJWTHeader],
  canCall: [generateAdminJWTHeader],
};

describe('Test the phone API endpoint', () => {
  it(...testCreate(Phone, {
    ...config,
  }));

  it(...testCreateUnicity(Phone, {
    ...config,
    requestCallBack: (r) => r
      .set(...generateAdminJWTHeader()),
    transformObject: {
      id: '_id',
      label: 'label',
      number: 'number',
      imei: 'imei',
      campus: { id: '_id', name: 'name' },
      model: { id: '_id', label: 'label' },
    },
  }));

  it(...testList(Phone, {
    ...config,
  }));

  it(...testDelete(Phone, {
    ...config,
    route: ({ id }) => `${config.route}/${id}`,
  }));

  it(...testGet(Phone, {
    ...config,
    route: ({ id }) => `${config.route}/${id}`,
  }));

  it(...testUpdate(Phone, {
    ...config,
    route: ({ id }) => `${config.route}/${id}`,
  }));

  it(...testBatch(Phone, {
    ...config,
    route: `${config.route}/batch`,
    queryParams: {},
    canCall: [generateSuperAdminJWTHeader],
  }));
});
