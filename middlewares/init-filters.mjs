/**
 * Simple middleware to ensure that ctx.filters exists.
 *
 * @param {Object} ctx KoaJS context
 * @param {function} next KoaJS next
 * @returns {Promise<void>}
 */
const initFiltersMiddleware = async (ctx, next) => {
  ctx.filters = ctx.filters || {};
  await next();
};
export default initFiltersMiddleware;
