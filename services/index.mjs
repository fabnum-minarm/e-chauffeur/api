import config from './config';
import MongooseService from './mongoose';
import SecretTokensService from './secret-tokens';
import './luxon';

export default Promise.all([
  MongooseService(config.get('mongodb')),
  SecretTokensService(),
]);
