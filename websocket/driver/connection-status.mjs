export default (socket) => {
  socket.on('driverConnected', ({ campusId, driverId }) => {
    // eslint-disable-next-line no-param-reassign
    socket.driverId = driverId;
    // eslint-disable-next-line no-param-reassign
    socket.campusId = campusId;

    const room = socket.in(`campus/${campusId}`);
    room.emit('driverConnection', driverId);
  });
  socket.on('disconnect', () => {
    if (socket.driverId && socket.campusId) {
      const room = socket.in(`campus/${socket.campusId}`);
      room.emit('driverDisconnection', socket.driverId);
    }
  });
  socket.on('pingDrivers', ({ driversIds, campusId }) => {
    driversIds.forEach((id) => {
      socket.in(`driver/${id}`).emit('pingDriver', { driverId: id, campusId });
    });
  });
};
