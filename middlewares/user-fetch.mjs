import User from '../models/user';
import APIError from '../helpers/api-error';

export default async ({ state }, next) => {
  if (state.user) {
    try {
      Object.assign(state.user, await User.findById(state.user.id).lean());
    } catch (e) {
      throw new APIError(403, 'Inconsistency in user fetch');
    }
  }

  await next();
};
