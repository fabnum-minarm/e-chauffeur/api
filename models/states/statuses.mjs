export const DRAFTED = 'drafted';
export const CREATED = 'created';
export const VALIDATED = 'validated'; // Regulation validation
export const REJECTED_BOUNDARY = 'rejected_boundary'; // Regulation validation
export const REJECTED_CAPACITY = 'rejected_capacity'; // Regulation validation
export const STARTED = 'started';
export const WAITING = 'waiting';
export const IN_PROGRESS = 'progress';
export const DELIVERED = 'delivered';
export const NONE = 'none';
export const BOOKED = 'booked';
export const WARNED = 'warned';
export const IN = 'in';
export const OUT = 'out';
export const CANCELED = 'canceled';
export const CANCELED_TECHNICAL = 'canceled_technical';
export const CANCELED_REQUESTED_CUSTOMER = 'canceled_requested_customer';
export const CANCELED_CUSTOMER_OVERLOAD = 'canceled_customer_overload';
export const CANCELED_CUSTOMER_MISSING = 'canceled_customer_missing';
export const CANCELED_STATUSES = [
  CANCELED,
  CANCELED_TECHNICAL,
  CANCELED_REQUESTED_CUSTOMER,
  CANCELED_CUSTOMER_OVERLOAD,
  CANCELED_CUSTOMER_MISSING,
];
