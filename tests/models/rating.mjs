import { nanoid } from 'nanoid';
import mongoose from 'mongoose';
import Rating from '../../models/rating';

const { Types: { ObjectId } } = mongoose;

export const generateDummyRating = async (params) => {
  if (!params.campus) {
    throw new Error('Provide campus in generateDummyRating');
  }

  return {
    _id: new ObjectId(),
    uxGrade: 0,
    recommandationGrade: 1,
    message: `Rating from test -- ${nanoid()}`,
    ...params,
  };
};

export default Rating;
