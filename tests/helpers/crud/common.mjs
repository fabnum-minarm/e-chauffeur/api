/* eslint-disable-next-line import/prefer-default-export */
export const defaultRouteName = (Model) => (Model.getDashedName && Model.getDashedName())
  || Model.modelName.toLowerCase();

export const syncProcessAsyncArrayOp = async (array, fn) => {
  // eslint-disable-next-line no-restricted-syntax
  for (const [index, value] of array.entries()) {
    // eslint-disable-next-line no-await-in-loop
    await fn(value, index);
  }
};
