import mongoose from 'mongoose';
import StateMachine from 'javascript-state-machine';
import pino from 'pino';
import Luxon from 'luxon';
import {
  SHUTTLE_COLLECTION_NAME, SHUTTLE_MODEL_NAME, WEEKLY, MONTHLY, STEP_MODEL_NAME,
} from './helpers/constants';
import { BOOKED, CANCELED_STATUSES, CREATED } from './states/statuses';
import manageCarAvailabilities from './helpers/car-availability-handler';
import cleanObjectPlugin from './helpers/object-cleaner';
import formatPhoneNumber from '../helpers/format-phone-number';
import stateMachinePlugin from './helpers/state-machine-plugin';
import APIError from '../helpers/api-error';
import User, { DRIVING_LICENSES, PUBLIC_TRANSPORT_LICENSE } from './user';
import ShuttleFactory from './shuttle-factory';
import Campus from './campus';
import Car from './car';
import {
  compareTokens, generateCampusFilter, getClientURL, withinFilter,
} from './helpers/custom-methods';
import { sendSMS } from '../services/twilio';
import {
  generateShuttleStateMachine,
  PASSENGER_CANCELABLE,
  CANCELABLE,
  ACTIVE_PASSENGER_STATUSES,
} from './states/shuttle';
import recurrencyPlugin from './helpers/recurrency-plugin';

const { Schema, model, Types } = mongoose;
const log = pino();
const { DateTime } = Luxon;

const ShuttleSchema = new Schema({
  _id: {
    type: Types.ObjectId,
    default: () => Types.ObjectId(),
    alias: 'id',
  },
  label: {
    type: String,
    required: true,
  },
  status: { type: String, default: CREATED },
  currentStopIndex: { type: Number, default: 0 },
  statusChanges: [{
    _id: false,
    status: { type: String, required: true },
    time: Date,
  }],
  start: {
    type: Date,
    required: true,
  },
  end: Date,
  campus: {
    _id: {
      type: String,
      required: true,
      alias: 'campus.id',
    },
    name: String,
  },
  shuttleFactory: {
    _id: {
      type: Types.ObjectId,
      alias: 'shuttleFactory.id',
    },
    label: {
      type: String,
    },
    stops: [{
      _id: { type: Types.ObjectId, required: true, alias: 'id' },
      poiId: { type: String, required: true },
      label: { type: String, required: true },
      time: { type: Date, required: true },
      reachDuration: { type: Number, required: true },
      location: {
        type: {
          type: String,
          enum: ['Point'],
        },
        coordinates: [Number],
      },
    }],
  },
  passengers: [{
    _id: {
      type: Types.ObjectId,
      alias: 'id',
      default() { return new Types.ObjectId(); },
    },
    email: {
      type: String,
      required: true,
    },
    firstname: String,
    lastname: String,
    phone: String,
    departure: {
      _id: {
        type: Types.ObjectId,
        required: true,
        alias: 'departure.id',
      },
      poiId: {
        type: String,
        required: true,
      },
      label: {
        type: String,
      },
    },
    arrival: {
      _id: {
        type: Types.ObjectId,
        required: true,
        alias: 'arrival.id',
      },
      poiId: {
        type: String,
        required: true,
      },
      label: {
        type: String,
      },
    },
    status: {
      type: String,
      default: BOOKED,
    },
    statusChanges: [{
      _id: false,
      status: { type: String, required: true },
      time: Date,
    }],
    count: {
      type: Number,
      default: 1,
    },
  }],
  driver: {
    _id: {
      type: Schema.ObjectId,
      alias: 'driver.id',
    },
    firstname: String,
    lastname: String,
    licenses: [{
      type: String,
      enum: DRIVING_LICENSES,
    }],
  },
  car: {
    _id: { type: String, alias: 'car.id' },
    label: String,
    model: {
      _id: { type: String },
      label: { type: String },
      capacity: { type: Number },
    },
  },
  comments: String,
  recurrence: {
    enabled: Boolean,
    withData: Boolean,
    frequency: {
      type: String,
      enum: [null, WEEKLY, MONTHLY],
    },
    nextHop: {
      _id: { type: Types.ObjectId, alias: 'recurrence.nextHop.id' },
      start: Date,
      end: Date,
      createdAt: Date,
    },
    previousHop: {
      _id: { type: Types.ObjectId, alias: 'recurrence.previousHop.id' },
      start: Date,
      end: Date,
      createdAt: Date,
    },
  },
}, { timestamps: true });

ShuttleSchema.plugin(cleanObjectPlugin, SHUTTLE_MODEL_NAME);
ShuttleSchema.plugin(recurrencyPlugin, {
  modelName: SHUTTLE_MODEL_NAME,
  refs: ['shuttleFactory', 'campus', 'label'],
});
ShuttleSchema.plugin(stateMachinePlugin(() => function initStateMachine() {
  const isLastStop = this.currentStopIndex === this.shuttleFactory.stops.length - 2;
  const state = generateShuttleStateMachine(isLastStop);
  state.init = this.status;
  StateMachine.apply(this, state);
}));

ShuttleSchema.pre('validate', async function beforeSave() {
  if (!this.start) {
    throw new APIError(400, 'Shuttle need a start date');
  }

  if (this.recurrence && this.recurrence.enabled && !this.recurrence.frequency) {
    throw new APIError(400, 'Frequency is required when recurrence is enabled.');
  }

  const campus = await Campus.findById(this.campus._id);
  if (!campus) {
    throw new APIError(404, 'Campus not found');
  }

  this.campus = campus;

  const shuttleFactory = await ShuttleFactory.findById(this.shuttleFactory._id).lean();
  if (!shuttleFactory) {
    throw new APIError(404, 'Pattern not found');
  }

  let previousTime = this.start;
  this.shuttleFactory.stops = await Promise.all(this.shuttleFactory.stops.map(async (stop, index) => {
    if (
      (stop._id && !stop._id.equals(shuttleFactory.stops[index]._id))
        || stop.label !== shuttleFactory.stops[index].label) {
      throw new APIError(404, 'Id or label not found');
    }

    if (index !== 0) {
      previousTime = DateTime.fromJSDate(previousTime)
        .plus({ minutes: shuttleFactory.stops[index].reachDuration })
        .toJSDate();
    }

    return {
      _id: stop._id,
      poiId: stop.poiId,
      label: stop.label,
      time: previousTime,
      location: stop.location,
      reachDuration: shuttleFactory.stops[index].reachDuration,
    };
  }));

  this.end = this.shuttleFactory.stops[this.shuttleFactory.stops.length - 1].time;

  const activePassengers = this.passengers.filter(({ status }) => !CANCELED_STATUSES.includes(status));

  if (this.passengers.length > 0) {
    this.passengers.forEach((p) => {
      if (this.passengers.reduce((acc, passenger) => (
        (passenger.email === p.email && ACTIVE_PASSENGER_STATUSES.includes(passenger.status)) ? acc + 1 : acc), 0) > 1
      ) {
        throw new APIError(400, 'You already booked a seat in this shuttle');
      }
    });

    if (!this.car || !this.car.model || !this.car.model.capacity) {
      throw new APIError(400, 'Car capacity must be provided');
    }

    let total = 0;
    shuttleFactory.stops.forEach(({ _id }) => {
      total += activePassengers.filter(({ departure }) => departure._id.equals(_id)).length;
      total -= activePassengers.filter(({ arrival }) => arrival._id.equals(_id)).length;
      if (total > this.car.model.capacity) {
        throw new APIError(400, 'Passengers number higher than car capacity');
      }
    });

    const passengers = await User.find({ email: { $in: this.passengers.map(({ email }) => email) } }).lean();

    this.passengers = this.passengers.map((passenger) => {
      const fetchedPassenger = passengers.find(({ email }) => email === passenger.email);
      if (fetchedPassenger) {
        let phone = '';
        if (passenger.phone) {
          phone = passenger.phone;
        } else if (fetchedPassenger.phone && fetchedPassenger.phone.canonical) {
          phone = fetchedPassenger.phone.canonical;
        }

        phone = phone && formatPhoneNumber(phone);

        return {
          ...fetchedPassenger,
          departure: passenger.departure,
          arrival: passenger.arrival,
          phone,
          status: passenger.status ? passenger.status : fetchedPassenger.status,
          statusChanges: passenger.statusChanges,
        };
      }
      return passenger;
    });
  }

  if (this.driver && this.driver._id) {
    const driver = await User.findById(this.driver._id);
    if (!driver) {
      throw new APIError(404, 'Driver not found');
    }
    if (!driver.licenses.includes(PUBLIC_TRANSPORT_LICENSE)) {
      throw new APIError(400, 'Wrong license');
    }

    this.driver = driver;
  }

  if (this.car && this.car._id) {
    const car = await Car.findById(this.car._id);
    if (!car) {
      throw new APIError(404, 'Car not found');
    }

    this.car = car;
  }
});

ShuttleSchema.post('save', async function postSave(shuttle) {
  // Here, we want to reassign param
  // eslint-disable-next-line no-param-reassign
  shuttle.steps = [];
  if (CREATED === shuttle.status) {
    await shuttle.persistSteps();
  }

  await manageCarAvailabilities(SHUTTLE_MODEL_NAME, shuttle);
  await this.createNextHop();
});

ShuttleSchema.methods.persistSteps = async function persistSteps() {
  const Step = mongoose.model(STEP_MODEL_NAME);
  const steps = await Step.generateStepsFromShuttle(this);
  this.steps = await Promise.all(steps);
  return this.steps;
};

ShuttleSchema.statics.generateFilters = function generateFilters(rawFilters, queryFilter, ...rest) {
  const filter = {
    ...rawFilters,
    ...queryFilter,
    ...this.withinFilter(queryFilter.start, queryFilter.end),
    ...rest,
  };
  delete filter.start;
  delete filter.end;

  if (filter.passengerId) {
    filter.passengers = { _id: filter.passengerId };
    delete filter.passengerId;
  }

  if (filter.current === 'false') {
    filter.$and = [
      { $or: filter.$or },
      {
        $or: [
          { status: { $nin: CANCELABLE } },
          { passengers: { $elemMatch: { ...filter.passengers, status: { $nin: PASSENGER_CANCELABLE } } } },
        ],
      },
    ];
    delete filter.passengers;
    delete filter.$or;
  } else if (filter.current === 'true') {
    filter.status = { $in: CANCELABLE };
    filter.passengers = { $elemMatch: { ...filter.passengers, status: { $in: PASSENGER_CANCELABLE } } };
  }

  delete filter.current;
  return filter;
};

ShuttleSchema.statics.generateCampusFilter = generateCampusFilter;
ShuttleSchema.statics.withinFilter = withinFilter;

ShuttleSchema.statics.findWithin = function findWithin(...params) {
  return this.find(this.generateFilters(...params));
};

ShuttleSchema.statics.countDocumentsWithin = function countDocumentsWithin(...params) {
  return this.countDocuments(this.generateFilters(...params));
};

ShuttleSchema.methods.sendSeveralSMS = async function sendUsersSMS(body) {
  await Promise.all(this.passengers.map(async ({ phone }) => {
    try {
      if (phone) {
        return await sendSMS(formatPhoneNumber(phone), body);
      }
    } catch (e) {
      // Silent error
      log.error(e);
    }
    return null;
  }));
};

ShuttleSchema.methods.sendPassengerSMS = async function sendPassengerSMS(passengerId, body) {
  await Promise.all(this.passengers.map(async ({ _id, phone }) => {
    if (phone && _id === passengerId) {
      try {
        return await sendSMS(phone, body);
      } catch (e) {
        // Silent error
        log.error(e);
      }
    }
    return null;
  }));
};

ShuttleSchema.methods.compareTokens = compareTokens;
ShuttleSchema.methods.getClientURL = getClientURL;

export default model(SHUTTLE_MODEL_NAME, ShuttleSchema, SHUTTLE_COLLECTION_NAME);
