import mongoose from 'mongoose';
import { RIDE_MODEL_NAME, SHUTTLE_MODEL_NAME } from './constants';
import { CANCELED_STATUSES } from '../states/statuses';

const checkUserAvailability = async function isUserAvailable(userId) {
  let id = userId;
  if (typeof id === 'string') {
    id = mongoose.Types.ObjectId(id);
  }

  const Ride = mongoose.model(RIDE_MODEL_NAME);
  const rides = await Ride.find({
    'owner._id': id,
    status: { $nin: CANCELED_STATUSES },
    $or: [
      { $and: [{ start: { $gte: this.start } }, { start: { $lte: this.end } }] },
      { $and: [{ end: { $gte: this.start } }, { end: { $lte: this.end } }] },
      { $and: [{ start: { $lte: this.start } }, { end: { $gte: this.end } }] },
    ],
  });

  const Shuttle = mongoose.model(SHUTTLE_MODEL_NAME);
  const departure = new Date(this.departure.time);
  const arrival = new Date(this.arrival.time);
  const shuttles = await Shuttle.aggregate([
    { $unwind: '$passengers' },
    {
      $match: {
        'passengers._id': id,
        'passengers.status': { $nin: CANCELED_STATUSES },
        status: { $nin: CANCELED_STATUSES },
      },
    },
    {
      $project: {
        _id: 0,
        start: {
          $reduce: {
            input: '$shuttleFactory.stops',
            initialValue: '',
            in: {
              $cond: [
                { $eq: ['$$this._id', '$passengers.departure._id'] },
                '$$this.time',
                '$$value',
              ],
            },

          },
        },
        end: {
          $reduce: {
            input: '$shuttleFactory.stops',
            initialValue: '',
            in: {
              $cond: [
                { $eq: ['$$this._id', '$passengers.arrival._id'] },
                '$$this.time',
                '$$value',
              ],
            },
          },
        },
      },
    },
    {
      $match: {
        $or: [
          { $and: [{ start: { $gte: departure } }, { start: { $lte: arrival } }] },
          { $and: [{ end: { $gte: departure } }, { end: { $lte: arrival } }] },
          { $and: [{ start: { $lte: departure } }, { end: { $gte: arrival } }] },
        ],
      },
    },
  ]);

  return rides.concat(shuttles).length === 0;
};

export default checkUserAvailability;
