import mongoose from 'mongoose';
import lGet from 'lodash.get';
import Luxon from 'luxon';
import { nanoid } from 'nanoid';
import { CAN_ACCESS_OWN_DATA_ON_RIDE, CAN_ACCESS_PERSONAL_DATA_ON_RIDE } from './rights';
import {
  DRAFTED, CREATED,
  CANCELED_STATUSES, VALIDATED,
} from './states/statuses';
import stateMachine, { CANCELABLE } from './states/ride';
import config from '../services/config';
import { sendSMS } from '../services/twilio';
import manageCarAvailabilities from './helpers/car-availability-handler';
import createdAtPlugin from './helpers/created-at';
import cleanObjectPlugin from './helpers/object-cleaner';
import stateMachinePlugin from './helpers/state-machine-plugin';
import formatPhoneNumber from '../helpers/format-phone-number';
import {
  CAMPUS_MODEL_NAME, CAR_MODEL_MODEL_NAME, CAR_MODEL_NAME, GEO_TRACKING_MODEL_NAME,
  POI_MODEL_NAME, RIDE_COLLECTION_NAME,
  RIDE_MODEL_NAME, STEP_MODEL_NAME,
  USER_MODEL_NAME,
} from './helpers/constants';
import {
  compareTokens, generateCampusFilter, getClientURL, withinFilter,
} from './helpers/custom-methods';
import APIError from '../helpers/api-error';
import { DRIVING_LICENSES } from './user';
import checkUserAvailabilities from './helpers/check-user-availability';

const DEFAULT_TIMEZONE = config.get('default_timezone');
const { DateTime, Duration } = Luxon;
const { Schema, Types } = mongoose;

function isCancelled(status) {
  return CANCELED_STATUSES.includes(status);
}

function isValidated(status) {
  return ![DRAFTED, CREATED].includes(status) && !isCancelled(status);
}

const RideSchema = new Schema({
  token: {
    type: String,
    default: () => nanoid(12),
  },
  status: { type: String, default: DRAFTED },
  statusChanges: [{
    _id: false,
    status: { type: String, required: true },
    time: Date,
  }],
  category: {
    _id: { type: String, alias: 'category.id' },
    label: String,
  },
  start: {
    type: Date,
    required: true,
  },
  end: Date,
  owner: {
    _id: {
      type: mongoose.Types.ObjectId,
      alias: 'owner.id',
    },
    firstname: {
      type: String,
      canEmit: [CAN_ACCESS_PERSONAL_DATA_ON_RIDE, CAN_ACCESS_OWN_DATA_ON_RIDE],
    },
    lastname: {
      type: String,
      canEmit: [CAN_ACCESS_PERSONAL_DATA_ON_RIDE, CAN_ACCESS_OWN_DATA_ON_RIDE],
    },
    email: {
      type: String,
      canEmit: [CAN_ACCESS_PERSONAL_DATA_ON_RIDE, CAN_ACCESS_OWN_DATA_ON_RIDE],
    },
  },
  departure: {
    _id: { type: String, alias: 'departure.id' },
    label: String,
    address: String,
    location: {
      type: {
        type: String,
        enum: ['Point'],
      },
      coordinates: {
        type: [Number],
      },
    },
  },
  arrival: {
    _id: { type: String, alias: 'arrival.id' },
    label: String,
    address: String,
    location: {
      type: {
        type: String,
        enum: ['Point'],
      },
      coordinates: {
        type: [Number],
      },
    },
  },
  driver: {
    _id: { type: Schema.ObjectId, alias: 'driver.id' },
    firstname: String,
    lastname: String,
    licenses: [{
      type: String,
      enum: DRIVING_LICENSES,
    }],
  },
  car: {
    _id: { type: String, alias: 'car.id' },
    label: String,
    model: {
      _id: { type: String },
      label: { type: String },
      capacity: { type: Number },
    },
  },
  campus: {
    _id: { type: String, required: true, alias: 'campus.id' },
    name: String,
    phone: {
      drivers: String,
      everybody: String,
    },
    timezone: {
      type: String,
      default: process.env.TZ || DEFAULT_TIMEZONE,
    },
    defaultReservationScope: Number,
    defaultRideDuration: Number,
  },
  comments: String,
  userComments: {
    type: String,
    canEmit: [CAN_ACCESS_PERSONAL_DATA_ON_RIDE, CAN_ACCESS_OWN_DATA_ON_RIDE],
  },
  passengersCount: {
    type: Number,
    default: 1,
  },
  passengersList: [{
    name: String,
  }],
  phone: String,
  luggage: {
    type: Boolean,
    default: false,
  },
});

RideSchema.plugin(createdAtPlugin);
RideSchema.plugin(cleanObjectPlugin, RIDE_MODEL_NAME);
RideSchema.plugin(stateMachinePlugin(), { stateMachine });

RideSchema.pre('validate', async function beforeSave() {
  if (this.start >= this.end) {
    throw new APIError(400, 'End date should be higher than start date');
  }

  if ((this.status !== DRAFTED || this.state === CREATED) && (!this.departure._id || !this.arrival._id)) {
    throw new APIError(400, 'Departure and arrival must be provided');
  }

  if (isValidated(this.status) && !this.car._id) {
    throw new APIError(400, 'Car must be provided');
  }

  if (this.departure.id && this.arrival.id) {
    if (this.departure.id === this.arrival.id) {
      throw new APIError(400, 'Departure and arrival should be different');
    }
  }

  try {
    this.phone = formatPhoneNumber(this.phone);
  } catch (e) {
    // Silent error
  }

  if (typeof this.driver === 'undefined' || !this.driver._id) {
    this.driver = null;
  }

  await Promise.all([
    (async (Campus) => {
      const campus = await Campus.findById(this.campus._id);
      if (campus) {
        if (this.status === DRAFTED) {
          this.end = DateTime
            .fromJSDate(this.start)
            .plus(Duration.fromObject({ minutes: campus.defaultRideDuration || 30 }))
            .toJSDate();

          const campusOpeningHour = campus.workedHours.start;
          const campusEndingHour = campus.workedHours.end;
          const rideStartingHour = DateTime.fromJSDate(this.start).toFormat('H');
          if (rideStartingHour < campusOpeningHour || rideStartingHour >= campusEndingHour) {
            throw new APIError(403, 'Ride start should be in campus worked hours');
          }
        }

        const currentReservationScope = DateTime.local()
          .plus({ seconds: campus.defaultReservationScope })
          .toJSDate();
        if (currentReservationScope < this.start) {
          throw new APIError(403, 'Ride date should be in campus reservation scope');
        }

        this.campus = campus;
      } else {
        throw new APIError(404, 'Campus not found');
      }
    })(mongoose.model(CAMPUS_MODEL_NAME)),
    (async (User) => {
      const userId = this.owner._id;
      if (!userId) {
        return;
      }
      const owner = await User.findById(userId).lean();

      const isUserAvailable = await checkUserAvailabilities.call(this, userId);
      const isNew = await mongoose.model(RIDE_MODEL_NAME).findById(this._id);

      if (!isNew && !isUserAvailable) {
        throw new APIError(400, 'User has already booked a ride or a shuttle');
      }

      const phone = lGet(owner, 'phone.canonical', null);
      this.owner = owner;
      if (phone && !this.phone && lGet(owner, 'phone.confirmed', false)) {
        this.phone = phone;
      }
    })(mongoose.model(USER_MODEL_NAME)),
    (async (Car, CarModel) => {
      if (this.driver._id && !this.car._id) {
        throw new APIError(400, 'Car must be provided');
      }
      const carId = this.car._id;
      this.car = await Car.findById(carId).lean();
      this.car.model = await CarModel.findById(this.car.model._id).lean();
    })(mongoose.model(CAR_MODEL_NAME), mongoose.model(CAR_MODEL_MODEL_NAME)),
    (async (Poi) => {
      if (!this.departure.address && !this.arrival.address) {
        const pois = await Poi.find({ _id: { $in: [this.arrival._id, this.departure._id] } });
        this.arrival = pois.find(({ _id }) => _id === this.arrival._id);
        this.departure = pois.find(({ _id }) => _id === this.departure._id);
      }
    })(mongoose.model(POI_MODEL_NAME)),
  ]);

  if (this.car._id && this.status !== DRAFTED && !isCancelled(this.status)) {
    const carCapacity = this.car.model.capacity || 3;
    if (this.passengersCount > carCapacity) {
      throw new APIError(400, 'Passenger count is higher than car capacity');
    }
  }
});

RideSchema.post('save', async (ride) => {
  // Here, we want to reassign param
  // eslint-disable-next-line no-param-reassign
  ride.steps = [];
  if (ride.status === VALIDATED) {
    await ride.persistSteps();
  }

  await manageCarAvailabilities(RIDE_MODEL_NAME, ride);
});

RideSchema.statics.castId = (v) => {
  try {
    return new Types.ObjectId(v);
  } catch (e) {
    return new Types.ObjectId(Buffer.from(v, 'base64').toString('hex'));
  }
};

RideSchema.statics.formatFilters = function formatFilters(rawFilters, queryFilter) {
  let filter = {
    ...rawFilters,
    ...queryFilter,
    ...this.filtersWithin(queryFilter.start, queryFilter.end),
  };

  delete filter.start;
  delete filter.end;

  if (filter.current) {
    const operator = filter.current === 'false' ? '$nin' : '$in';

    filter = {
      ...filter,
      status: { [operator]: CANCELABLE },
    };

    delete filter.current;
  }

  if (!filter) {
    return null;
  }
  return filter;
};

RideSchema.statics.filtersWithin = withinFilter;
RideSchema.statics.generateCampusFilter = generateCampusFilter;

RideSchema.statics.findWithin = function findWithin(...params) {
  return this.find(this.formatFilters(...params));
};

RideSchema.statics.countDocumentsWithin = function countDocumentsWithin(...params) {
  return this.countDocuments(this.formatFilters(...params));
};

RideSchema.methods.findDriverPosition = async function findDriverPosition() {
  const GeoTracking = mongoose.model(GEO_TRACKING_MODEL_NAME);
  const [position = null] = await GeoTracking.aggregate([
    {
      $match: { 'driver._id': this.driver._id },
    },
    { $unwind: '$positions' },
    {
      $project: {
        driver: '$driver._id',
        position: '$positions.location',
        date: '$positions._id',
      },
    },
    { $sort: { date: -1 } },
    { $limit: 1 },
  ]).allowDiskUse(true);
  return position;
};

RideSchema.methods.persistSteps = async function persistSteps() {
  const Step = mongoose.model(STEP_MODEL_NAME);
  const steps = await Step.generateStepsFromRide(this);
  this.steps = await Promise.all(steps);
  return this.steps;
};

RideSchema.methods.sendSMS = async function sendUserSMS(body) {
  try {
    if (this.phone) {
      return await sendSMS(this.phone, body);
    }
  } catch (e) {
    // Silent error
    // eslint-disable-next-line no-console
    console.error(e);
  }
  return null;
};

RideSchema.methods.compareTokens = compareTokens;
RideSchema.methods.getClientURL = getClientURL;

RideSchema.methods.getSatisfactionQuestionnaireURL = function getSatisfactionQuestionnaireURL() {
  return `${config.get('user_website_url')}/rating?rideId=${this.id}`;
};

export default mongoose.model(RIDE_MODEL_NAME, RideSchema, RIDE_COLLECTION_NAME);
