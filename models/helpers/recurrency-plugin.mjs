import mongoose from 'mongoose';
import Luxon from 'luxon';
import config from '../../services/config';
import { MONTHLY, WEEKLY } from './constants';

const { DateTime } = Luxon;

function recurrencyPlugin(schema, { modelName, refs }) {
  /* eslint-disable no-param-reassign */
  schema.methods.createNextHop = async function createNextHop() {
    const recurrencyUnit = config.get('recurrency_frequency:unit');
    const recurrencyValue = config.get('recurrency_frequency:value');

    const recurrencyScope = DateTime.local().plus({ [recurrencyUnit]: recurrencyValue }).toJSDate();
    let start = DateTime.fromJSDate(this.start);

    if (!this.recurrence.enabled || (this.recurrence.nextHop || {})._id || start >= recurrencyScope) {
      return null;
    }
    const Model = mongoose.model(modelName);

    let end = DateTime.fromJSDate(this.end);
    let timeToAdd;

    switch (this.recurrence.frequency) {
      case WEEKLY:
        timeToAdd = { week: 1 };
        break;
      case MONTHLY:
        timeToAdd = { month: 1 };
        break;
      default:
        break;
    }

    start = start.plus(timeToAdd);
    end = end.plus(timeToAdd);

    this.recurrence.nextHop = await Model.create({
      ...refs.reduce((acc, key) => ({ ...acc, [key]: this[key] }), {}),
      start: start.toJSDate(),
      end: end.toJSDate(),
      recurrence: {
        previousHop: this,
        enabled: this.recurrence.enabled,
        withData: this.recurrence.withData,
        frequency: this.recurrence.frequency,
      },
    });

    await this.save();
    return this.recurrence.nextHop;
  };

  schema.statics.createNextHop = async function createNextHop(id) {
    const document = await this.findOne(typeof id === 'string' ? mongoose.Types.ObjectId(id) : id);
    return document.createNextHop();
  };

  schema.statics.findSlotsToCopy = async function findSlotsToCopy() {
    const recurrencyUnit = config.get('recurrency_frequency:unit');
    const recurrencyValue = config.get('recurrency_frequency:value');

    const criteria = {
      'recurrence.enabled': true,
      'recurrence.nextHop': null,
      start: { $lt: DateTime.local().plus({ [recurrencyUnit]: recurrencyValue }).toJSDate() },
    };
    const documents = await Promise.all([
      this.find({
        ...criteria,
        'recurrence.frequency': WEEKLY,
      }),
      this.find({
        ...criteria,
        'recurrence.frequency': MONTHLY,
      }),
    ]);
    return [].concat(...documents);
  };
}

export default recurrencyPlugin;
