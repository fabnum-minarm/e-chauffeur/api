import { nanoid } from 'nanoid';
import { createClient } from './redis';
import config from './config';

const SECRET_TOKENS_CHANNEL = 'secret-refresh-orders';
const SECRET = 'secret';
export const secretTokens = [config.get('token:secret')];
const tokenExpiration = config.get('token:duration');

export default async function secretTokensService() {
  const subscriptionClient = createClient();
  const retrieveClient = createClient();
  if (subscriptionClient && retrieveClient) {
    subscriptionClient.subscribe(SECRET_TOKENS_CHANNEL);
    const fetchSecrets = async function fetchSecrets() {
      const keys = (await retrieveClient.keys(`${SECRET}_*`)).sort();
      if (keys.length > 0) {
        const tokens = await retrieveClient.mget(...keys);
        secretTokens.splice(0, secretTokens.length, ...tokens);
      }
    };
    await fetchSecrets();
    subscriptionClient.on('message', fetchSecrets);
    return true;
  }
  return false;
}

export const pubNewSecret = async function pubNewSecret() {
  const recordClient = createClient();
  const pubClient = createClient();

  if (recordClient && pubClient) {
    const token = nanoid(128);
    const key = `${SECRET}_${(new Date()).toISOString()}`;
    await recordClient
      .pipeline()
      .set(key, token)
      .expire(key, tokenExpiration)
      .exec();
    await pubClient.publish(SECRET_TOKENS_CHANNEL, token);
    return true;
  }
  return false;
};
