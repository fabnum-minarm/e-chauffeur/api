import Prometheus from 'prom-client';
import http from 'http';
import config from './services/config';

const port = config.get('prometheus_exporter');
if (port) {
  http.createServer((req, res) => {
    Prometheus.register.metrics().then((metrics) => {
      res.writeHead(200, { 'Content-Type': Prometheus.register.contentType });
      res.end(metrics);
    }).catch(() => {
      res.writeHead(500);
      res.end();
    });
  }).listen(port || 9091, '0.0.0.0');
}
