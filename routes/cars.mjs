import generateCRUD from '../helpers/abstract-route';
import Car from '../models/car';
import Ride from '../models/ride';
import {
  CAN_CREATE_CAR, CAN_EDIT_CAR, CAN_GET_CAR, CAN_LIST_CAR, CAN_REMOVE_CAR,
} from '../models/rights';
import { csvToJson, validateCampus } from '../middlewares/csv-to-json';
import contentNegociation from '../middlewares/content-negociation';
import maskOutput from '../middlewares/mask-output';
import searchQuery from '../middlewares/search-query';
import { filtersFromParams } from '../middlewares/query-helper';
import Campus from '../models/campus';

const router = generateCRUD(Car, {
  create: {
    right: CAN_CREATE_CAR,
  },
  list: {
    right: CAN_LIST_CAR,
    middlewares: [
      contentNegociation,
      maskOutput,
      searchQuery,
      filtersFromParams('campus._id', 'campus_id'),
    ],
    main: async (ctx) => {
      if (ctx.query.filters && ctx.query.filters.start && ctx.query.filters.end) {
        const { sort, license } = ctx.query.filters;
        const LAST_DRIVER_RIDE_ID = 'last-driver-ride';
        const start = new Date(ctx.query.filters.start);
        const end = new Date(ctx.query.filters.end);

        let lastRidedCar = null;
        let driver = null;
        let filters = {};

        if (sort && sort[LAST_DRIVER_RIDE_ID]) {
          driver = sort[LAST_DRIVER_RIDE_ID];
          const [ride] = await Ride.find({ 'driver._id': sort[LAST_DRIVER_RIDE_ID] }).sort({ $natural: -1 }).limit(1);
          lastRidedCar = ride && ride.car ? ride.car : null;
        }

        if (license) {
          filters = {
            ...filters,
            ...Car.generateLicenseFilter(license),
          };
        }

        let sortedCars = await Campus.findCars(ctx.params.campus_id, start, end, driver, filters);
        if (lastRidedCar && sortedCars.find(({ id }) => id === lastRidedCar.id)) {
          sortedCars = [
            lastRidedCar,
            ...sortedCars.filter(({ id }) => id !== lastRidedCar.id),
          ];
        }

        ctx.body = sortedCars;
      } else {
        const { offset, limit } = ctx.parseRangePagination(Car, { max: 1000 });

        const [total, data] = await Promise.all([
          Car.countDocuments(ctx.filters),
          Car.find(ctx.filters).skip(offset).limit(limit),
        ]);

        ctx.setRangePagination(Car, {
          total, offset, count: data.length, limit,
        });
        ctx.body = data;
      }
    },
  },
  get: {
    right: CAN_GET_CAR,
  },
  delete: {
    right: CAN_REMOVE_CAR,
  },
  update: {
    right: CAN_EDIT_CAR,
  },
  batch: {
    right: CAN_CREATE_CAR,
    refs: ['_id', 'label'],
    middlewares: [
      csvToJson,
      validateCampus,
    ],
  },
});

export default router;
