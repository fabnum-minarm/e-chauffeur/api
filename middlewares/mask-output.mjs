import mask from 'json-mask';
import transform from 'lodash.transform';
import isPlainObject from 'lodash.isplainobject';

/**
 * Transform object, using .toObject or .toCleanObject
 *
 * @param e
 * @param {Object} ctx KoaJS context
 * @returns {Object}
 */
export function cleanObject(e, ctx) {
  if (!e) {
    return e;
  }

  let item = e;
  if (e.toCleanObject) {
    item = e.toCleanObject({ virtuals: true, aliases: true }, ctx);
  } else if (e.toObject) {
    item = e.toObject({ virtuals: true, aliases: true });
  }

  if (!isPlainObject(item) && !Array.isArray(item)) {
    return item;
  }

  return transform(item, (acc, val, key) => {
    if (['__v'].includes(key)) {
      return;
    }
    acc[key.replace ? key.replace(/^_/g, '') : key] = cleanObject(val, ctx);
  });
}

/**
 * Middleware to add an automatic scoping, thanks to json-mask library
 * It will check the `mask` query param
 *
 * @param {Object} ctx KoaJS context
 * @param {function} next KoaJS next
 * @returns {Promise<void>}
 */
const maskOutputMiddleware = async (ctx, next) => {
  await next();
  if (typeof ctx.body === 'object') {
    ctx.body = cleanObject(ctx.body, ctx);
    ctx.body = mask(ctx.body, (ctx.query || {}).mask || ',');
  }
};

export default maskOutputMiddleware;
