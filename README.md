# E-Chauffeur - API 
[![pipeline status](https://gitlab.com/fabnum-minarm/e-chauffeur/api/badges/develop/pipeline.svg)](https://gitlab.com/fabnum-minarm/e-chauffeur/api/-/commits/develop)
[![coverage report](https://gitlab.com/fabnum-minarm/e-chauffeur/api/badges/develop/coverage.svg)](https://gitlab.com/fabnum-minarm/e-chauffeur/api/-/commits/develop)

This project is the backend of e-Chauffeur.

## Dependencies

* Redis to synchronize websockets (optional)
* MongoDB to store data
* Prometheus to analyze metrics

## Swagger

[Go to swagger](swagger/index.html)
