import Router from '@koa/router';
import { ensureThatFiltersExists } from '../../../middlewares/query-helper';
import resolveRights from '../../../middlewares/check-rights';
import { CAN_LIST_CAMPUS_DRIVER_RIDE } from '../../../models/rights';
import maskOutput from '../../../middlewares/mask-output';
import Campus from '../../../models/campus';

const router = new Router();
router.get(
  '/',
  resolveRights(CAN_LIST_CAMPUS_DRIVER_RIDE),
  maskOutput,
  ensureThatFiltersExists('status'),
  async (ctx) => {
    const { filters } = ctx.query;

    ctx.body = await Campus.findRidesWithStatus(ctx.params.driver_id, filters.status);
  },
);

export default router;
