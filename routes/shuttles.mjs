import mask from 'json-mask';
import camelCase from 'lodash.camelcase';
import generateCRUD from '../helpers/abstract-route';
import Shuttle from '../models/shuttle';
/*
* Rides rights are almost equal to shuttles rights
* */
import {
  CAN_CREATE_RIDE,
  CAN_GET_RIDE,
  CAN_GET_OWNED_RIDE,
  CAN_GET_RIDE_WITH_TOKEN,
  CAN_DELETE_SHUTTLE,
  CAN_LIST_SHUTTLE,
  CAN_EDIT_SHUTTLE,
  CAN_EDIT_SHUTTLE_PASSENGERS,
  CAN_EDIT_SHUTTLE_STATUS,
  CAN_EDIT_OWNED_SHUTTLE_STATUS,
} from '../models/rights';
import maskOutput, { cleanObject } from '../middlewares/mask-output';
import { ensureThatFiltersExists, filtersFromParams } from '../middlewares/query-helper';
import { prefetchMiddleware } from '../helpers/prefetch-document';
import resolveRights from '../middlewares/check-rights';
import { CANCELED_REQUESTED_CUSTOMER, CANCELED_STATUSES, NONE } from '../models/states/statuses';
import { BOOK, CANCEL_REQUESTED_CUSTOMER } from '../models/states/actions';
import { loadPassengerStateMachine } from '../models/states/shuttle';
import checkUserAvailability from '../models/helpers/check-user-availability';
import APIError from '../helpers/api-error';

const SHUTTLE_UPDATE_EVENT = 'shuttleUpdate';
const STEPS_UPDATE_EVENT = 'stepsUpdate';
const CLEAN_STEPS_EVENT = 'cleanSteps';

function ioEmit(ctx, data, eventName = '', rooms = []) {
  let { app: { io } } = ctx;
  rooms.forEach((room) => {
    io = io.in(room);
  });
  io.emit(eventName, data);
}

const router = generateCRUD(Shuttle, {
  create: {
    right: [CAN_CREATE_RIDE],
    middlewares: [
      maskOutput,
      async (ctx, next) => {
        await next();
        const shuttle = ctx.body;

        ioEmit(ctx, cleanObject(ctx.body), SHUTTLE_UPDATE_EVENT, [
          // @todo check ns purpose
          // `ride/${shuttle.id}`,
          `campus/${shuttle.campus.id}`,
        ]);

        ioEmit(ctx, shuttle.steps.map(cleanObject), STEPS_UPDATE_EVENT, [`driver/${shuttle.driver.id}`]);
      },
    ],
  },
  list: {
    right: [CAN_LIST_SHUTTLE],
    middlewares: [
      maskOutput,
      filtersFromParams('campus._id', 'campus_id'),
      ensureThatFiltersExists('start', 'end'),
    ],
    async main(ctx) {
      const { offset, limit } = ctx.parseRangePagination(Shuttle, { max: 1000 });
      const total = await Shuttle.countDocumentsWithin(ctx.filters, ctx.query.filters);
      const data = await Shuttle.findWithin(ctx.filters, ctx.query.filters).skip(offset).limit(limit);
      ctx.setRangePagination(Shuttle, {
        total, offset, count: data.length, limit,
      });

      ctx.body = data;
      ctx.log.info(
        { filters: ctx.filters, offset, limit },
        `Find query in ${Shuttle.modelName}`,
      );
    },
  },
  get: {
    preMiddlewares: [prefetchMiddleware(Shuttle)],
    right: [CAN_GET_RIDE, CAN_GET_OWNED_RIDE, CAN_GET_RIDE_WITH_TOKEN],
    middlewares: [maskOutput],
  },
  update: {
    preMiddlewares: [prefetchMiddleware(Shuttle)],
    right: [CAN_EDIT_SHUTTLE, CAN_EDIT_SHUTTLE_PASSENGERS],
    async main(ctx) {
      let { body } = ctx.request;
      const { id } = ctx.params;
      const shuttle = ctx.getPrefetchedDocument(id, Shuttle);

      if (CANCELED_STATUSES.includes(shuttle.status)) {
        ctx.throw_and_log(403, 'Shuttle has already been cancelled');
      }

      if (!ctx.may(CAN_EDIT_SHUTTLE)) {
        body = mask(body, 'passengers');
      }

      if (shuttle.passengers.length < body.passengers.length) {
        const newPassengerIndex = body.passengers.findIndex(({ status }) => !status);
        const passenger = body.passengers[newPassengerIndex];

        if (passenger.id) {
          const isPassengerAvailable = await checkUserAvailability.call(passenger, passenger.id);
          if (!isPassengerAvailable) {
            throw new APIError(400, 'User has already booked a ride or a shuttle');
          }
        }

        const activePassengers = shuttle.passengers.filter(({ status }) => !CANCELED_STATUSES.includes(status));
        let total = 0;
        shuttle.shuttleFactory.stops.forEach(({ _id }) => {
          total += activePassengers.filter(({ departure }) => departure._id.equals(_id)).length;
          total -= activePassengers.filter(({ arrival }) => arrival._id.equals(_id)).length;
          if (total >= shuttle.car.model.capacity) {
            throw new APIError(400, 'Passengers number higher than car capacity');
          }
        });

        const defaultStatus = { status: NONE, statusChanges: [] };
        const updatedPassenger = Object.assign(passenger, defaultStatus);
        await loadPassengerStateMachine(updatedPassenger);
        await passenger[camelCase(BOOK)]();
        shuttle.passengers.set(newPassengerIndex, updatedPassenger);
      }

      delete body.status;

      shuttle.set(body);
      ctx.body = await shuttle.save();
      ctx.log.info(`${Shuttle.modelName} "${id}" has been modified`);

      ioEmit(ctx, cleanObject(ctx.body), SHUTTLE_UPDATE_EVENT, [
        // @todo check ns purpose
        // `ride/${shuttle.id}`,
        `campus/${shuttle.campus.id}`,
      ]);
      ioEmit(ctx, ctx.body.steps.map(cleanObject), STEPS_UPDATE_EVENT, [`driver/${shuttle.driver.id}`]);
    },
  },
  delete: {
    right: CAN_DELETE_SHUTTLE,
  },
});

router.post(
  '/:id/:action',
  prefetchMiddleware(Shuttle),
  resolveRights(CAN_EDIT_SHUTTLE_STATUS, CAN_EDIT_OWNED_SHUTTLE_STATUS),
  maskOutput,
  async (ctx) => {
    const { params: { id, action } } = ctx;

    if (!ctx.may(CAN_EDIT_SHUTTLE_STATUS) && action !== CANCEL_REQUESTED_CUSTOMER) {
      ctx.throw_and_log(403, `You're not authorized to mutate to "${action}"`);
    }

    const shuttle = ctx.getPrefetchedDocument(id, Shuttle);
    if (!shuttle) {
      ctx.throw_and_log(404, `${Shuttle.modelName} "${id}" not found`);
    }

    if (shuttle.cannot(action)) {
      ctx.throw_and_log(400, `State violation : shuttle cannot switch to "${action}"`);
    }

    await shuttle[camelCase(action)]();

    ctx.body = await shuttle.save();
    ctx.log.info(
      { shuttle },
      `${Shuttle.modelName} "${id}" has been modified`,
    );

    // @todo handle socket event for user app
    ioEmit(ctx, cleanObject(ctx.body), SHUTTLE_UPDATE_EVENT, [
      `campus/${shuttle.campus.id}`,
    ]);

    if (CANCELED_STATUSES.includes(ctx.body.status)) {
      ioEmit(ctx, { id: shuttle.id, type: 'shuttle' }, CLEAN_STEPS_EVENT, [`driver/${shuttle.driver.id}`]);
    } else {
      ioEmit(ctx, ctx.body.steps.map(cleanObject), STEPS_UPDATE_EVENT, [`driver/${shuttle.driver.id}`]);
    }
  },
);

router.post(
  '/:id/:action/:userId',
  prefetchMiddleware(Shuttle),
  resolveRights(CAN_EDIT_SHUTTLE_STATUS, CAN_EDIT_OWNED_SHUTTLE_STATUS),
  maskOutput,
  async (ctx) => {
    const { params: { id, action, userId } } = ctx;

    if (!ctx.may(CAN_EDIT_SHUTTLE_STATUS) && action !== CANCEL_REQUESTED_CUSTOMER) {
      ctx.throw_and_log(403, `You're not authorized to mutate to "${action}"`);
    }

    const shuttle = ctx.getPrefetchedDocument(id, Shuttle);
    if (!shuttle) {
      ctx.throw_and_log(404, `${Shuttle.modelName} "${id}" not found`);
    }

    const passengerIndex = shuttle.passengers.findIndex(({ _id, status }) => (
      _id.equals(userId) && status !== CANCELED_REQUESTED_CUSTOMER));

    if (passengerIndex === -1) {
      ctx.throw_and_log(404, 'Passenger not found');
    }

    const passenger = shuttle.passengers[passengerIndex];

    await loadPassengerStateMachine(passenger);

    if (passenger.cannot(action)) {
      ctx.throw_and_log(400, `State violation : passenger cannot switch to "${action}"`);
    }
    await passenger[camelCase(action)]();

    shuttle.passengers.set(passengerIndex, passenger);

    ctx.body = await shuttle.save();
    ctx.log.info(
      { shuttle },
      `User ${passenger.id} in ${Shuttle.modelName} "${id}" has been modified`,
    );

    // @todo handle socket event for user app
    ioEmit(ctx, cleanObject(ctx.body), SHUTTLE_UPDATE_EVENT, [
      `campus/${shuttle.campus.id}`,
    ]);
    ioEmit(ctx, ctx.body.steps.map(cleanObject), STEPS_UPDATE_EVENT, [`driver/${shuttle.driver.id}`]);
  },
);

export default router;
