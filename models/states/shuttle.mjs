import Luxon from 'luxon';
import get from 'lodash.get';
import StateMachine from 'javascript-state-machine';
import pino from 'pino';
import camelCase from 'lodash.camelcase';

import config from '../../services/config';
import Car from '../car';
import { sendSMS } from '../../services/twilio';
import formatPhoneNumber from '../../helpers/format-phone-number';
import {
  CREATED, STARTED, DELIVERED, BOOKED, WARNED, IN, OUT,
  CANCELED, CANCELED_TECHNICAL, CANCELED_REQUESTED_CUSTOMER, CANCELED_STATUSES, NONE,
} from './statuses';
import {
  START, STOP, END, WARN, GET_IN, GET_OUT,
  CANCEL, CANCEL_TECHNICAL, CANCEL_REQUESTED_CUSTOMER, deleteRelatedSteps, BOOK,
} from './actions';
import { sendSatisfactionMail, sendShuttleValidationMail } from '../../services/mail';

const log = pino();
const DEFAULT_TIMEZONE = config.get('default_timezone');
const { DateTime } = Luxon;

function show(path) {
  return get(this, path, '');
}

function getStart() {
  return DateTime.fromJSDate(this.start)
    .setZone(get(this, 'campus.timezone', DEFAULT_TIMEZONE))
    .toLocaleString(DateTime.DATETIME_SHORT);
}

export const PASSENGER_CANCELABLE = [BOOKED];
const PASSENGER_CAN_GET_IN = [BOOKED, WARNED];
export const ACTIVE_PASSENGER_STATUSES = [...PASSENGER_CAN_GET_IN, IN, OUT];

export const passengerStateMachine = {
  init: NONE,
  transitions: [
    { name: BOOK, from: NONE, to: BOOKED },
    { name: WARN, from: BOOKED, to: WARNED },
    { name: GET_IN, from: PASSENGER_CAN_GET_IN, to: IN },
    { name: GET_OUT, from: IN, to: OUT },
    { name: CANCEL_REQUESTED_CUSTOMER, from: PASSENGER_CANCELABLE, to: CANCELED_REQUESTED_CUSTOMER },
  ],
  methods: {
    async onEnterState({ from, to }) {
      if (this.status === from) {
        this.status = to;
        this.statusChanges.push({
          status: to,
          time: new Date(),
        });

        const phone = this.phone && formatPhoneNumber(this.phone);

        switch (to) {
          case BOOKED:
            if (this.email) {
              await sendShuttleValidationMail(this.email, {
                data: {
                  departure: this.departure.label,
                  arrival: this.arrival.label,
                },
              });
            } else if (phone) {
              await sendSMS(
                phone,
                `Votre place dans la navette au départ de ${this.departure.label} et en direction de ${
                  this.arrival.label} est réservée`,
              );
            }
            break;
          case WARNED:
            if (phone) {
              try {
                await sendSMS(phone, `Bonjour ${this.email}, votre navette est à l'arrêt précédent`);
              } catch (e) {
                log.error(e);
              }
            }
            break;
          case CANCELED_REQUESTED_CUSTOMER:
            if (phone) {
              try {
                await sendSMS(
                  phone,
                  `Nous vous confirmons l'annulation de votre place dans la navette au départ de ${
                    this.departure.label}`,
                );
              } catch (e) {
                log.error(e);
              }
            }
            break;
          case OUT: {
            const satisfactionURL = `${config.get('user_website_url')}/rating?shuttleId=${this.shuttleId}`;
            if (this.email) {
              await sendSatisfactionMail('e-Navette')(this.email, {
                data: { satisfactionURL },
              });
            } else if (phone) {
              try {
                await this.sendSMS(
                  phone,
                  'Merci d\'avoir fait appel à notre offre de mobilité. '
                  + `Vous pouvez évaluer le service e-Navette : ${satisfactionURL}`,
                );
              } catch (e) {
                log.error(e);
              }
            }
            break;
          }
          default:
            break;
        }
      }
    },
  },
};

export const CANCELABLE = [CREATED, STARTED];
export const loadPassengerStateMachine = (passenger) => {
  const state = passengerStateMachine;
  state.init = passenger.status || NONE;
  return StateMachine.apply(passenger, state);
};

export const generateShuttleStateMachine = (isLastStop) => ({
  init: CREATED,
  transitions: [
    { name: START, from: CREATED, to: STARTED },
    {
      name: STOP,
      from: STARTED,
      to() {
        if (isLastStop) {
          return DELIVERED;
        }
        return STARTED;
      },
    },
    { name: END, from: STARTED, to: DELIVERED },
    { name: CANCEL, from: CANCELABLE, to: CANCELED },
    { name: CANCEL_TECHNICAL, from: CANCELABLE, to: CANCELED_TECHNICAL },
  ],
  methods: {
    async onTransition({ from, to }) {
      if (from === this.status) {
        this.statusChanges.push({
          status: to,
          time: new Date(),
        });

        if (CANCELED_STATUSES.includes(to)) {
          await deleteRelatedSteps({ 'shuttle._id': this._id });
          await Car.findOneAndUpdate(
            { 'unavailabilities.displacement._id': this._id },
            { $pull: { unavailabilities: { 'displacement._id': this._id } } },
          );
        }

        const firePassengerAction = async (passenger, action, shuttleId) => {
          await loadPassengerStateMachine(Object.assign(passenger, { shuttleId }));
          let updatedPassenger;

          try {
            updatedPassenger = passenger[camelCase(action)]();
          } catch (e) {
            log.error(e);
          }
          return updatedPassenger;
        };

        switch (to) {
          case CANCELED_TECHNICAL:
            await this.sendSeveralSMS(
              `Un problème technique nous oblige à annuler votre navette ${
                show.call(this, 'label')} du ${getStart.call(this)}`,
            );
            break;
          case CANCELED:
            await this.sendSeveralSMS(
              `Votre navette ${show.call(this, 'label')} du ${getStart.call(this)} a été annulée`,
            );
            break;
          case STARTED: {
            const currentStop = this.shuttleFactory.stops[this.currentStopIndex];
            const nextStop = this.shuttleFactory.stops[this.currentStopIndex + 1];

            const {
              passengersToWarn, passengersIn, passengersOut, otherPassengers,
            } = this.passengers.reduce((acc, passenger) => {
              let type = 'otherPassengers';
              if (!CANCELED_STATUSES.includes(passenger.status)) {
                if (passenger.departure.id.equals(nextStop.id)) {
                  type = 'passengersToWarn';
                } else if (passenger.departure.id.equals(currentStop.id)) {
                  type = 'passengersIn';
                } else if (passenger.arrival.id.equals(currentStop.id)) {
                  type = 'passengersOut';
                }
              }
              acc[type].push(passenger);
              return acc;
            }, {
              passengersToWarn: [],
              passengersIn: [],
              passengersOut: [],
              otherPassengers: [],
            });

            this.passengers = await Promise.all([
              ...passengersToWarn.map((passenger) => firePassengerAction(passenger, WARN, this._id)),
              ...passengersIn.map((passenger) => firePassengerAction(passenger, GET_IN, this._id)),
              ...passengersOut.map((passenger) => firePassengerAction(passenger, GET_OUT, this._id)),
              ...otherPassengers,
            ]);
            break;
          }
          case DELIVERED: {
            await Car.findOneAndUpdate(
              { 'unavailabilities.displacement._id': this._id },
              { $pull: { unavailabilities: { 'displacement._id': this._id } } },
            );

            const passengersOut = this.passengers.filter(({ status }) => status === IN);
            const otherPassengers = this.passengers.filter(({ status }) => status !== IN);

            this.passengers = await Promise.all([
              ...passengersOut.map((passenger) => firePassengerAction(passenger, GET_OUT, this._id)),
              ...otherPassengers,
            ]);
            break;
          }
          default:
            break;
        }
      }
    },
  },
});
