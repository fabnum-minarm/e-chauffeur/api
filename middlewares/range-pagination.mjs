import qs from 'qs';

/**
 * Custom header used for range request negotiation : accept-ranges
 *
 * @type {string}
 */
export const HEADER_ACCEPT_RANGES = 'X-Accept-Ranges';

/**
 * Custom header used for range request negotiation : content-range
 *
 * @type {string}
 */
export const HEADER_CONTENT_RANGES = 'X-Content-Range';

/**
 * Custom header used for range request negotiation : range
 *
 * @type {string}
 */
export const HEADER_RANGE = 'x-range';

/**
 * Add functions to context, to permit range request negotiation
 *
 * @param {Object} ctx KoaJS context
 * @param {function} next KoaJS next
 * @returns {Promise<void>}
 */
const rangePaginationMiddleware = async (ctx, next) => {
  ctx.setRangePagination = (entity, {
    total,
    offset,
    count,
    limit = 30,
  }) => {
    const name = (entity.getDashedName && entity.getDashedName()) || entity.modelName.toLowerCase();
    ctx.set(HEADER_ACCEPT_RANGES, name);
    ctx.set(HEADER_CONTENT_RANGES, `${name} ${offset}-${Math.max(offset + count - 1, 0)}/${total}#${limit}`);
  };

  ctx.parseRangePagination = (entity, { max = 30 } = {}) => {
    const name = (entity.getDashedName && entity.getDashedName()) || entity.modelName.toLowerCase();
    const range = qs.parse(ctx.headers[HEADER_RANGE] || '')[name] || '';
    const [lower, higher] = range.split('-').map((n) => parseInt(n, 10));
    return {
      offset: lower || 0,
      limit: Math.min(!Number.isNaN(higher) ? higher - lower + 1 : max) || max,
    };
  };

  await next();
};

export default rangePaginationMiddleware;
