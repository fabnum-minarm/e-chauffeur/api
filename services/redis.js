import Redis from 'ioredis';
import config from './config';

const instances = [];

export function createClient() {
  if (config.get('redis')) {
    const instance = new Redis(config.get('redis'), {
      retryStrategy(times) {
        return Math.min(Math.exp(times), 20000);
      },
    });

    instances.push(instance);
    return instance;
  }
  return null;
}

export const pubClient = createClient();
export const subClient = createClient();

export const disconnectAllClients = async function disconnectAllClients() {
  return Promise.all(instances.map((i) => i.quit()));
};
