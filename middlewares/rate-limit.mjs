import config from '../services/config';
import RateLimit from '../models/rate-limit';

/**
 * Middleware to log request and add a rate limit on the route.
 *
 * @param {Object} ctx KoaJS context
 * @param {function} next KoaJS next
 * @returns {Promise<void>}
 */
const rateLimitMiddleware = async (ctx, next) => {
  const { headers, body } = ctx.request;
  let { ip } = ctx.request;

  if (body && body.email) {
    const forwardedIP = headers['x-forwarded-for'];
    if (forwardedIP) {
      ip = forwardedIP;
    }
    const ref = body.email;
    const userRateLimit = await RateLimit.countDocuments({ ref, ip });
    if (userRateLimit && userRateLimit >= config.get('rate_limit:attempt_number')) {
      ctx.throw_and_log(429, `Too many request for ${ref} with ip : ${ip}`);
    }
  } else {
    ctx.throw_and_log(400);
  }

  await next();
};

export default rateLimitMiddleware;
