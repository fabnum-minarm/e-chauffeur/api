import { cleanObject } from './mask-output';

/**
 * Quick method to send an event to many rooms.
 *
 * @param {Object} ctx KoaJS context
 * @param {Object} data Payload
 * @param {string} eventName Name of the event
 * @param {string[]} rooms Rooms names
 */
export function ioEmit(ctx, data, eventName = '', rooms = []) {
  let { app: { io } } = ctx;
  rooms.forEach((room) => {
    io = io.in(room);
  });
  io.emit(eventName, data);
}

/**
 * Middleware to send event to room types
 *
 * @param {string} eventName
 * @param {string[]} roomTypes
 * @returns {function(...[*])}
 */
const ioEmitMiddleware = (eventName, roomTypes) => async (ctx, next) => {
  await next();
  const { body } = ctx;
  const rooms = roomTypes.reduce((acc, type) => {
    if (['ride', 'shuttle'].includes(type)) {
      acc.push(`${type}/${body.id}`);
    } else if (body[type] && body[type].id) {
      acc.push(`${type}/${body[type].id}`);
    }
    return acc;
  }, []);

  ioEmit(ctx, cleanObject(ctx.body), eventName, rooms);
};

export default ioEmitMiddleware;
