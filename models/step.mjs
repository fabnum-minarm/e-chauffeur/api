import mongoose from 'mongoose';
import orderBy from 'lodash.orderby';
import {
  STEP_MODEL_NAME, STEP_COLLECTION_NAME,
} from './helpers/constants';
import config from '../services/config';
import { countDocumentsWithin, findWithin } from './helpers/custom-methods';
import { REJECTED_BOUNDARY, REJECTED_CAPACITY, CANCELED_STATUSES } from './states/statuses';

const { Schema, Types } = mongoose;
const DEFAULT_TIMEZONE = config.get('default_timezone');

export const STEP_CHANGE_CAR = 'ChangeCar';
export const STEP_GET_PASSENGER = 'GetPassenger';
export const STEP_WAIT_PASSENGER = 'WaitPassenger';
export const STEP_DRIVE_PASSENGER = 'DrivePassenger';
export const STEP_DROP_PASSENGER = 'DropPassenger';
export const STEP_GO_TO_STOP = 'GoToStop';

const RIDE_STEPS = [
  STEP_GET_PASSENGER,
  STEP_WAIT_PASSENGER,
  STEP_DRIVE_PASSENGER,
  STEP_DROP_PASSENGER,
];

const ACTIONS_ORDER = [
  STEP_CHANGE_CAR,
  STEP_GET_PASSENGER,
  STEP_WAIT_PASSENGER,
  STEP_DRIVE_PASSENGER,
  STEP_DROP_PASSENGER,
  STEP_GO_TO_STOP,
];

const StepSchema = new Schema({
  _id: {
    required: true,
    type: String,
  },
  type: {
    required: true,
    type: String,
    enum: ['shuttle', 'ride'],
  },
  action: {
    type: String,
  },
  location: {
    _id: {
      type: Types.ObjectId,
      required: true,
      alias: 'location.id',
    },
    poiId: { type: String, required: true },
    label: String,
    address: String,
    location: {
      type: {
        type: String,
        enum: ['Point'],
      },
      coordinates: {
        type: [Number],
      },
    },
  },
  date: {
    type: Date,
    required: true,
  },
  driver: {
    _id: { type: Schema.ObjectId, alias: 'ride.driver.id' },
    firstname: String,
    lastname: String,
  },
  car: {
    _id: { type: String, alias: 'ride.car.id' },
    label: String,
    model: {
      _id: { type: String },
      label: { type: String },
      capacity: { type: Number },
    },
  },
  campus: {
    _id: { type: String, required: true, alias: 'ride.campus.id' },
    phone: { drivers: String },
    timezone: {
      type: String,
      default: process.env.TZ || DEFAULT_TIMEZONE,
    },
  },
  ride: {
    _id: Types.ObjectId,
    comments: String,
    passengersCount: {
      type: Number,
      default: 1,
    },
    phone: String,
    luggage: {
      type: Boolean,
      default: false,
    },
  },
  shuttle: {
    _id: Types.ObjectId,
    shuttleFactory: {
      _id: Types.ObjectId,
      stops: [{
        _id: {
          type: Types.ObjectId,
          required: true,
          alias: 'id',
        },
        poiId: { type: String, required: true },
        label: { type: String, required: true },
        time: { type: Date, required: true },
        reachDuration: { type: Number, required: true },
        location: {
          type: {
            type: String,
            enum: ['Point'],
          },
          coordinates: [Number],
        },
      }],
    },
    passengers: [{
      _id: Types.ObjectId,
      email: {
        type: String,
        required: true,
      },
      firstname: String,
      lastname: String,
      phone: {
        canonical: String,
        original: String,
      },
      departure: {
        _id: {
          type: Types.ObjectId,
          required: true,
          alias: 'departure.id',
        },
        poiId: {
          type: String,
          required: true,
        },
        label: {
          type: String,
        },
      },
      arrival: {
        _id: {
          type: Types.ObjectId,
          required: true,
          alias: 'arrival.id',
        },
        poiId: {
          type: String,
          required: true,
        },
        label: {
          type: String,
        },
      },
      count: {
        type: Number,
        default: 1,
      },
    }],
    comments: String,
  },
  done: {
    type: Date,
    expires: 3600 * 24,
  },
}, { timestamps: true });

StepSchema.loadClass(class StepClass {
  queryOptions = {
    upsert: true,
    new: true,
    omitUndefined: true,
  }

  static async generateStepsFromRide(ride) {
    const Step = this;

    const options = {
      upsert: true,
      new: true,
      omitUndefined: true,
    };

    const data = {
      ride,
      type: 'ride',
      campus: ride.campus,
      driver: ride.driver,
      car: ride.car,
    };

    if ([...CANCELED_STATUSES, REJECTED_BOUNDARY, REJECTED_CAPACITY].includes(ride.status)) {
      data.done = new Date();
    }

    const steps = RIDE_STEPS.map((action) => {
      const isDepartureStep = [STEP_GET_PASSENGER, STEP_WAIT_PASSENGER].includes(action);
      const location = isDepartureStep ? ride.departure : ride.arrival;

      return {
        ...data,
        _id: `R${ride.id}${action}`,
        action,
        date: isDepartureStep ? ride.start : ride.end,
        location: {
          _id: new Types.ObjectId(),
          poiId: location._id,
          label: location.label,
          location: location.location,
        },
      };
    });

    const changeCarSteps = await Step.generateChangeCarSteps(steps, data);

    return steps.concat(changeCarSteps).map((step) => Step.findOneAndUpdate({ _id: step._id }, step, options));
  }

  static async generateStepsFromShuttle(shuttle) {
    const Step = this;

    const options = {
      upsert: true,
      new: true,
      omitUndefined: true,
    };

    const data = {
      shuttle: {
        _id: shuttle._id,
        start: shuttle.start,
        departure: shuttle.departure,
        arrival: shuttle.arrival,
        passengers: shuttle.passengers.filter(({ status }) => !CANCELED_STATUSES.includes(status)),
      },
      type: 'shuttle',
      campus: shuttle.campus,
      driver: shuttle.driver,
      car: shuttle.car,
    };

    if ([...CANCELED_STATUSES, REJECTED_BOUNDARY, REJECTED_CAPACITY].includes(shuttle.status)) {
      data.done = new Date();
    }

    const steps = shuttle.shuttleFactory.stops.map((stop, i) => ({
      ...data,
      _id: `S${shuttle.id}${STEP_GO_TO_STOP}S${i}`,
      action: STEP_GO_TO_STOP,
      date: stop.time,
      location: stop,
    }));

    const changeCarSteps = await Step.generateChangeCarSteps(steps, data);

    return steps.concat(changeCarSteps).map((step) => Step.findOneAndUpdate({ _id: step._id }, step, options));
  }

  static async generateChangeCarSteps(steps, payload) {
    const Step = this;
    const displacement = payload[payload.type];

    const driverSteps = await Step.find({ 'driver._id': payload.driver._id }).lean();

    const sortedSteps = Step.orderSteps(driverSteps.concat(steps));

    return sortedSteps.reduce((acc, step, i) => {
      const stepIndex = steps.findIndex(({ _id }) => _id === step._id);
      if (i > 0 && stepIndex !== -1) {
        if (step.car._id !== sortedSteps[i - 1].car._id) {
          const _id = sortedSteps[i - 1].action === STEP_CHANGE_CAR
            ? sortedSteps[i - 1]._id
            : `R${displacement._id}${STEP_CHANGE_CAR}`;

          acc.push({
            ...payload,
            _id,
            action: STEP_CHANGE_CAR,
            date: displacement.start,
            location: steps[stepIndex].location,
          });
        }
      }
      return acc;
    }, []);
  }

  static orderSteps(steps, iteratees = ['date', 'actionIndex'], orders = ['asc', 'asc']) {
    return orderBy(
      steps.map((step) => ({
        ...step,
        actionIndex: ACTIONS_ORDER.findIndex((s) => s === step.action),
      })),
      iteratees,
      orders,
    );
  }
});

StepSchema.statics.filtersWithin = function filtersWithin(after, before, f = {}) {
  const filters = f;
  filters.date = {
    $lte: before,
    $gte: after,
  };
  return filters;
};
StepSchema.statics.findWithin = findWithin;
StepSchema.statics.countDocumentsWithin = countDocumentsWithin;

export default mongoose.model(STEP_MODEL_NAME, StepSchema, STEP_COLLECTION_NAME);
