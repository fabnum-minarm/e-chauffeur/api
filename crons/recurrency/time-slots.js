import TimeSlot from '../../models/time-slot';
import config from '../../services/config';

const timeSlotRecurrency = async (logger) => {
  const maxIterations = config.get('recurrency_frequency:value');

  let iterations = 0;
  while (iterations <= maxIterations) {
    // eslint-disable-next-line no-await-in-loop
    const slots = await TimeSlot.findSlotsToCopy();
    if (slots.length === 0) {
      logger.info('Time slots creations done');
      return;
    }
    // eslint-disable-next-line no-await-in-loop
    await Promise.all(slots.map((slot) => {
      logger.info(`Create next hop after time-slot "${slot._id}"`);
      return slot.createNextHop();
    }));
    iterations += 1;
  }
};

export default timeSlotRecurrency;
