/**
 * Add a function to context to throw error and log as an error
 *
 * @param {Object} ctx KoaJS context
 * @param {function} next KoaJS next
 * @returns {Promise<void>}
 */
const throwAndLogMiddleware = async (ctx, next) => {
  ctx.throw_and_log = (code, message) => {
    ctx.log.error(message);
    ctx.throw(code, typeof message === 'object' && !message.message ? JSON.stringify(message) : message);
  };

  await next();
};

export default throwAndLogMiddleware;
