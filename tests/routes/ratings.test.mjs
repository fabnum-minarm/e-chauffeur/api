import {
  testCreate, testList,
} from '../helpers/crud';
import Rating, { generateDummyRating } from '../models/rating';
import {
  generateSuperAdminJWTHeader,
  generateAdminJWTHeader,
  generateAnonymousJWTHeader,
} from '../request';
import Ride from '../models/ride';
import { createDummyCampus, generateDummyCampus } from '../models/campus';
import { createDummyPoi } from '../models/poi';
import { createDummyCarModel } from '../models/car-model';
import { createDummyCar } from '../models/car';

const campus = generateDummyCampus();

const config = {
  route: '/ratings',
  queryParams: {
    filters: {
      campus: campus._id,
    },
  },
  deletionRef: 'message',
  async generateDummyObject() {
    const toDropLater = [];
    const dummyCampus = await createDummyCampus(campus);
    toDropLater.push(dummyCampus);

    const dummyDeparture = await createDummyPoi();
    const dummyArrival = await createDummyPoi();
    toDropLater.push(dummyDeparture, dummyArrival);

    const dummyCarModel = await createDummyCarModel();
    const dummyCar = await createDummyCar({ model: dummyCarModel, campus: dummyCampus });
    toDropLater.push(dummyCarModel, dummyCar);

    const dummyRide = await Ride.create({
      start: new Date(),
      end: new Date(Date.now() + 1000),
      campus: dummyCampus,
      departure: dummyDeparture,
      arrival: dummyArrival,
      car: dummyCar,
    });
    toDropLater.push(dummyRide);

    const dummyRating = await generateDummyRating({ campus: dummyCampus, ride: dummyRide });

    return [dummyRating, toDropLater];
  },
};

describe('Test the rating API endpoint', async () => {
  it(...testCreate(Rating, {
    ...config,
    canCall: [generateAnonymousJWTHeader],
    expectedStatus: 204,
  }));

  it(...testList(Rating, {
    ...config,
    cannotCall: [generateAdminJWTHeader],
    canCall: [generateSuperAdminJWTHeader],
  }));
});
