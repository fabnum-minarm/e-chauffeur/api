import config from '../../services/config';
import Shuttle from '../../models/shuttle';

const shuttleRecurrency = async (logger) => {
  const maxIterations = config.get('recurrency_frequency:value');

  let iterations = 0;
  while (iterations <= maxIterations) {
    /* eslint-disable no-await-in-loop */
    const shuttles = await Shuttle.findSlotsToCopy();
    if (shuttles.length === 0) {
      logger.info('Shuttle creations done');
      return;
    }
    // eslint-disable-next-line no-await-in-loop
    await Promise.all(shuttles.map((shuttle) => {
      logger.info(`Create next hop after shuttle "${shuttle._id}"`);
      return shuttle.createNextHop();
    }));
    iterations += 1;
  }
};

export default shuttleRecurrency;
