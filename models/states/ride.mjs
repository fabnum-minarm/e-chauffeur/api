import Luxon from 'luxon';
import get from 'lodash.get';
import config from '../../services/config';

import Car from '../car';
import {
  DRAFTED,
  CREATED,
  VALIDATED,
  STARTED,
  WAITING,
  IN_PROGRESS,
  DELIVERED,
  REJECTED_BOUNDARY,
  REJECTED_CAPACITY,
  CANCELED,
  CANCELED_CUSTOMER_MISSING,
  CANCELED_REQUESTED_CUSTOMER,
  CANCELED_CUSTOMER_OVERLOAD,
  CANCELED_TECHNICAL,
  CANCELED_STATUSES,
} from './statuses';

import {
  CREATE,
  VALIDATE,
  START,
  WAIT,
  PROGRESS,
  DELIVER,
  REJECT_BOUNDARY,
  REJECT_CAPACITY,
  CANCEL,
  CANCEL_CUSTOMER_OVERLOAD,
  CANCEL_CUSTOMER_MISSING,
  CANCEL_TECHNICAL,
  CANCEL_REQUESTED_CUSTOMER,
  deleteRelatedSteps,
} from './actions';
import { sendRideValidationMail, sendSatisfactionMail } from '../../services/mail';

const DEFAULT_TIMEZONE = config.get('default_timezone');
const { DateTime } = Luxon;

export const CANCELABLE = [CREATED, VALIDATED, STARTED, WAITING, IN_PROGRESS];

export default {
  init: DRAFTED,
  transitions: [
    { name: CREATE, from: DRAFTED, to: CREATED },
    { name: VALIDATE, from: CREATED, to: VALIDATED },
    { name: REJECT_BOUNDARY, from: CREATED, to: REJECTED_BOUNDARY },
    { name: REJECT_CAPACITY, from: CREATED, to: REJECTED_CAPACITY },
    { name: START, from: VALIDATED, to: STARTED },
    { name: WAIT, from: STARTED, to: WAITING },
    { name: PROGRESS, from: WAITING, to: IN_PROGRESS },
    { name: DELIVER, from: IN_PROGRESS, to: DELIVERED },
    { name: CANCEL, from: CANCELABLE, to: CANCELED },
    { name: CANCEL_TECHNICAL, from: CANCELABLE, to: CANCELED_TECHNICAL },
    { name: CANCEL_REQUESTED_CUSTOMER, from: CANCELABLE, to: CANCELED_REQUESTED_CUSTOMER },
    { name: CANCEL_CUSTOMER_OVERLOAD, from: CANCELABLE, to: CANCELED_CUSTOMER_OVERLOAD },
    { name: CANCEL_CUSTOMER_MISSING, from: CANCELABLE, to: CANCELED_CUSTOMER_MISSING },
  ],

  methods: {
    async onEnterState({ from, to }) {
      if (from === this.status) {
        this.statusChanges.push({
          status: to,
          time: new Date(),
        });
        const show = (path) => get(this, path, '');
        const start = DateTime.fromJSDate(this.start)
          .setZone(get(this, 'campus.timezone', DEFAULT_TIMEZONE))
          .toLocaleString(DateTime.DATETIME_SHORT);
        const hasOwner = !!get(this, 'owner._id', false);

        if (CANCELED_STATUSES.includes(to)) {
          await deleteRelatedSteps({ 'ride._id': this._id });
          await Car.findOneAndUpdate(
            { 'unavailabilities.displacement._id': this._id },
            { $pull: { unavailabilities: { 'displacement._id': this._id } } },
          );
        }

        switch (to) {
          case VALIDATED:
            if (this.owner.email) {
              await sendRideValidationMail(this.owner.email, {
                data: {
                  departure: show('departure.label'),
                  arrival: show('arrival.label'),
                  start,
                },
              });
            } else if (this.phone) {
              await this.sendSMS(
                'Bonjour, '
                + `votre course de ${show('departure.label')} à ${show('arrival.label')} le `
                + `${start} est prise en compte.`
                + `Pour l'annuler, appelez le ${show('campus.phone.everybody')}.`,
              );
            }
            break;
          case REJECTED_CAPACITY:
            if (hasOwner) {
              await this.sendSMS(
                'Bonjour, '
                + `malheureusement, votre course de ${show('departure.label')} à ${show('arrival.label')} le `
                + `${start} ne peut pas être assurée pour des raisons techniques ou humaines. `
                + `En cas d'ugence, appelez le ${show('campus.phone.everybody')}.`,
              );
            }
            break;
          case REJECTED_BOUNDARY:
            if (hasOwner) {
              await this.sendSMS(
                'Bonjour, '
              + `malheureusement, votre course de ${show('departure.label')} à ${show('arrival.label')} le `
              + `${start} a été refusée. `
              + `En cas d'ugence, appelez le ${show('campus.phone.everybody')}.`,
              );
            }
            break;
          case STARTED:
            await this.sendSMS(
              `Votre chauffeur est en route (${show('car.model.label')} / ${show('car.id')}). `
              + `Suivez son arrivée : ${this.getClientURL()}`,
            );
            break;
          case WAITING:
            await this.sendSMS(
              `Votre chauffeur ${show('driver.firstname')} est arrivé au point de rencontre.`,
            );
            break;
          case DELIVERED:
            await Car.findOneAndUpdate(
              { 'unavailabilities.displacement._id': this._id },
              { $pull: { unavailabilities: { 'displacement._id': this._id } } },
            );

            if (this.owner.email) {
              await sendSatisfactionMail('e-Chauffeur')(this.owner.email, {
                data: {
                  satisfactionURL: this.getSatisfactionQuestionnaireURL(),
                },
              });
            } else if (this.phone) {
              await this.sendSMS(
                'Merci d\'avoir fait appel à notre offre de mobilité. '
                + `Vous pouvez évaluer le service e-Chauffeur : ${this.getSatisfactionQuestionnaireURL()}`,
              );
            }
            break;
          case CANCELED_TECHNICAL:
            await this.sendSMS(
              `Un problème technique nous oblige à annuler votre course vers ${show('arrival.label')} `
              + `le ${start}.`,
            );
            break;
          case CANCELED_REQUESTED_CUSTOMER:
            await this.sendSMS(
              `Nous confirmons l'annulation de la course vers ${show('arrival.label')} `
              + `le ${start}.`,
            );
            break;
          case CANCELED_CUSTOMER_MISSING:
            await this.sendSMS(
              `Suite à votre absence, votre course vers ${show('arrival.label')} `
              + `le ${start} a été annulée.`,
            );
            break;
          default:
            break;
        }
      }
    },
  },
};
