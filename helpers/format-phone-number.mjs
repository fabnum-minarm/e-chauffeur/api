import gliphone from 'google-libphonenumber';

const { PhoneNumberFormat, PhoneNumberUtil } = gliphone;
const REGION = 'FR';

export default (phone) => {
  const phoneUtil = PhoneNumberUtil.getInstance();
  const phoneNumberProto = phoneUtil.parse(phone, REGION);
  if (phoneUtil.isValidNumber(phoneNumberProto)) {
    return phoneUtil.format(phoneNumberProto, PhoneNumberFormat.E164);
  }
  return '';
};
