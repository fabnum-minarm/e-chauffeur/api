import Prometheus from 'prom-client';

/**
 * Prometheus histogram to index http requests durations.
 *
 * @type {Histogram<string>}
 */
const httpRequestDurationMicroseconds = new Prometheus.Histogram({
  name: 'http_request_duration_ms',
  help: 'Duration of HTTP requests in ms',
  labelNames: ['method', 'route', 'code'],
  buckets: [0.10, 5, 15, 50, 100, 200, 300, 400, 500],
});

/**
 * Record request duration to integrate this result to prometheus export.
 *
 * @param {Object} ctx KoaJS context
 * @param {function} next KoaJS next
 * @returns {Promise<void>}
 */
const prometheusMetricsMiddleware = async (ctx, next) => {
  const startEpoch = Date.now();
  await next();
  const responseTimeInMs = Date.now() - startEpoch;

  httpRequestDurationMicroseconds
    // eslint-disable-next-line no-underscore-dangle
    .labels(ctx.method, ctx._matchedRoute, ctx.status)
    .observe(responseTimeInMs);
};

export default prometheusMetricsMiddleware;
