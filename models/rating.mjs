import mongoose from 'mongoose';
import createdAtPlugin from './helpers/created-at';
import addCSVContentPlugin from './helpers/add-csv-content';
import Ride from './ride';
import { RATING_COLLECTION_NAME, RATING_MODEL_NAME } from './helpers/constants';
import APIError from '../helpers/api-error';
import Shuttle from './shuttle';

const { Schema } = mongoose;

const RatingSchema = new Schema({
  ride: {
    _id: {
      type: Schema.Types.ObjectId,
      alias: 'ride.id',
      index: {
        unique: true,
        sparse: true,
      },
    },
    campus: {
      _id: {
        type: String,
        alias: 'ride.campus.id',
      },
    },
  },
  shuttle: {
    _id: {
      type: Schema.Types.ObjectId,
      alias: 'shuttle.id',
      index: {
        unique: true,
        sparse: true,
      },
    },
    campus: {
      _id: {
        type: String,
        alias: 'shuttle.campus.id',
      },
    },
  },
  uxGrade: {
    type: Number,
    required: true,
  },
  recommandationGrade: {
    type: Number,
    required: true,
  },
  message: String,
});

RatingSchema.plugin(createdAtPlugin);
RatingSchema.plugin(addCSVContentPlugin);

RatingSchema.pre('validate', async function preValidate(next) {
  const ride = await Ride.findById(this.ride._id).lean();
  const shuttle = await Shuttle.findById(this.shuttle._id).lean();

  if (!ride && !shuttle) {
    throw new APIError(404, 'Displacement does not exist in database');
  }

  if (ride) {
    this.ride = ride;
  } else if (shuttle) {
    this.shuttle = shuttle;
  }

  next();
});

RatingSchema.statics.filtersWithin = function filtersWithin(start, end, f = {}) {
  const filters = f;
  filters.$and = [
    { createdAt: { $gte: start } },
    { createdAt: { $lte: end } },
  ];
  return filters;
};

RatingSchema.statics.generateCampusFilter = function generateCampusFilter(campuses) {
  if (campuses.length > 0) {
    return {
      $or: [].concat(campuses).map((campusId) => ({
        $or: [{ 'shuttle.campus._id': campusId }, { 'ride.campus._id': campusId }],
      })),
    };
  }
  return {};
};

export default mongoose.model(RATING_MODEL_NAME, RatingSchema, RATING_COLLECTION_NAME);
