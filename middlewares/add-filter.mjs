/**
 * Middleware factory to be aware of a filter from query and add it to ctx.filters
 *
 * @param {string} Query ID in querystring
 * @param {string} mongoPath Query to use, mongodb side
 * @returns {function(...[*])}
 */
const addFilterFactory = (id, mongoPath) => async (ctx, next) => {
  const queryFilters = ((ctx.query || {}).filters || {});
  const filters = ctx.filters || {};
  if (queryFilters[id]) {
    filters[mongoPath] = queryFilters[id];
    delete ctx.query.filters[id];
  }
  ctx.filters = filters;
  await next();
};

export default addFilterFactory;
