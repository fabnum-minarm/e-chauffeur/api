import mongoose from 'mongoose';
import createdAtPlugin from './helpers/created-at';
import recurrencyPlugin from './helpers/recurrency-plugin';
import {
  TIME_SLOT_COLLECTION_NAME,
  TIME_SLOT_DASHED_NAME,
  TIME_SLOT_MODEL_NAME,
  MONTHLY, WEEKLY,
} from './helpers/constants';
import { countDocumentsWithin, withinFilter, findWithin } from './helpers/custom-methods';
import APIError from '../helpers/api-error';

const { Schema } = mongoose;

const TimeSlotSchema = new Schema({
  start: {
    type: Date,
    required: true,
  },
  end: {
    type: Date,
    required: true,
  },
  cars: [{
    _id: { type: String, alias: 'id' },
    label: { type: String },
    model: {
      label: { type: String },
    },
  }],
  drivers: [{
    _id: { type: mongoose.Types.ObjectId, alias: 'id' },
    firstname: String,
    lastname: String,
  }],
  campus: {
    _id: { type: String, alias: 'campus.id' },
  },
  title: String,
  comments: String,
  recurrence: {
    enabled: Boolean,
    withData: Boolean,
    frequency: {
      type: String,
      enum: [null, WEEKLY, MONTHLY],
    },
    nextHop: {
      _id: { type: Schema.ObjectId, alias: 'recurrence.nextHop.id' },
      start: Date,
      end: Date,
      createdAt: Date,
    },
    previousHop: {
      _id: { type: Schema.ObjectId, alias: 'recurrence.previousHop.id' },
      start: Date,
      end: Date,
      createdAt: Date,
    },
  },
});

TimeSlotSchema.plugin(createdAtPlugin);
TimeSlotSchema.plugin(recurrencyPlugin, { modelName: TIME_SLOT_MODEL_NAME, refs: ['campus', 'cars', 'drivers'] });

TimeSlotSchema.pre('validate', async function preValidate(next) {
  if (this.recurrence && this.recurrence.enabled && !this.recurrence.frequency) {
    throw new APIError(400, 'Frequency is required when recurrence is enabled.');
  }
  next();
});

TimeSlotSchema.post('save', async function createNextHops() {
  await this.createNextHop();
});

TimeSlotSchema.statics.getDashedName = () => TIME_SLOT_DASHED_NAME;
TimeSlotSchema.statics.filtersWithin = withinFilter;
TimeSlotSchema.statics.findWithin = findWithin;
TimeSlotSchema.statics.countDocumentsWithin = countDocumentsWithin;

export default mongoose.model(TIME_SLOT_MODEL_NAME, TimeSlotSchema, TIME_SLOT_COLLECTION_NAME);
